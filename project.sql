-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 22, 2020 at 05:28 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `adonis_schema`
--

DROP TABLE IF EXISTS `adonis_schema`;
CREATE TABLE IF NOT EXISTS `adonis_schema` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  `migration_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `adonis_schema`
--

INSERT INTO `adonis_schema` (`id`, `name`, `batch`, `migration_time`) VALUES
(1, '1503248427885_user', 1, '2019-11-11 06:17:58'),
(2, '1503248427886_token', 1, '2019-11-11 06:17:58'),
(3, '1572323142287_project_schema', 1, '2019-11-11 06:17:58'),
(4, '1572323158826_feature_schema', 1, '2019-11-11 06:17:58'),
(5, '1572323174765_test_case_schema', 1, '2019-11-11 06:17:58'),
(6, '1573452978797_user_project_schema', 1, '2019-11-11 06:17:58'),
(7, '1573808068679_user_schema', 2, '2019-11-15 09:55:07'),
(8, '1575880452578_test_execution_plan_schema', 3, '2019-12-09 10:39:11'),
(9, '1575887134662_executions_has_users_schema', 3, '2019-12-09 10:39:11'),
(10, '1575887150601_executions_has_tes_cases_schema', 3, '2019-12-09 10:39:11'),
(11, '1576231966775_test_result_schema', 4, '2019-12-13 10:26:54'),
(12, '1576232043701_feature_has_test_excution_schema', 4, '2019-12-13 10:26:54'),
(13, '1576233198108_executions_has_tes_cases_schema', 5, '2019-12-13 10:34:44'),
(14, '1576470555110_test_cases_schema', 6, '2019-12-16 04:32:17'),
(15, '1576470697309_test_execution_plans_schema', 6, '2019-12-16 04:32:17'),
(16, '1577950632533_feature_has_test_excutions_schema', 7, '2020-01-02 07:38:46'),
(17, '1577950698363_executions_has_tes_cases_schema', 8, '2020-01-02 07:39:02'),
(18, '1587574470788_user_project_schema', 9, '2020-04-22 16:55:44'),
(19, '1587574760012_user_project_schema', 10, '2020-04-22 17:01:28'),
(20, '1587658154071_priority_setting_schema', 11, '2020-04-23 16:24:49'),
(21, '1587658186519_servity_setting_schema', 11, '2020-04-23 16:24:49'),
(22, '1587658236191_defect_status_setting_schema', 11, '2020-04-23 16:24:49'),
(23, '1587725788981_history_schema', 12, '2020-04-24 11:01:47'),
(24, '1587881119458_user_schema', 13, '2020-04-26 06:06:32'),
(25, '1587881242353_user_schema', 14, '2020-04-26 06:20:57'),
(26, '1587882183811_user_schema', 15, '2020-04-26 06:25:24'),
(27, '1587908061561_history_schema', 16, '2020-04-26 13:36:27'),
(28, '1587908078489_executions_has_tes_cases_schema', 16, '2020-04-26 13:36:27');

-- --------------------------------------------------------

--
-- Table structure for table `defect_status_settings`
--

DROP TABLE IF EXISTS `defect_status_settings`;
CREATE TABLE IF NOT EXISTS `defect_status_settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tep_id` int(10) UNSIGNED DEFAULT NULL,
  `defect_status_name` varchar(200) NOT NULL,
  `amount` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `defect_status_settings_defect_status_name_unique` (`defect_status_name`),
  KEY `defect_status_settings_tep_id_foreign` (`tep_id`),
  KEY `defect_status_settings_created_by_foreign` (`created_by`),
  KEY `defect_status_settings_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `defect_status_settings`
--

INSERT INTO `defect_status_settings` (`id`, `tep_id`, `defect_status_name`, `amount`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 'New1', NULL, 2, 2, 'A', '2020-04-29 19:53:14', '2020-05-04 09:49:37'),
(2, NULL, 'Reopen', NULL, 1, 1, 'A', '2020-04-29 19:53:22', '2020-04-29 19:53:22'),
(3, NULL, 'Inprogress - DEV', NULL, 1, 1, 'A', '2020-04-29 19:53:30', '2020-04-29 19:53:30'),
(4, NULL, 'Inprogress - QA', NULL, 1, 1, 'A', '2020-04-29 19:53:39', '2020-04-29 19:53:39'),
(5, NULL, 'Archived', NULL, 1, 1, 'A', '2020-04-29 19:53:48', '2020-04-29 19:53:48'),
(6, NULL, 'Non', NULL, 1, 1, 'A', '2020-05-04 09:48:10', '2020-05-04 09:48:10'),
(7, NULL, 'Archived1', NULL, 1, 1, 'A', '2020-05-04 12:39:24', '2020-05-04 12:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `executions_has_tes_cases`
--

DROP TABLE IF EXISTS `executions_has_tes_cases`;
CREATE TABLE IF NOT EXISTS `executions_has_tes_cases` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` int(10) NOT NULL,
  `tep_id` int(10) UNSIGNED DEFAULT NULL,
  `feature_id` int(10) UNSIGNED DEFAULT NULL,
  `test_cases_id` int(10) UNSIGNED DEFAULT NULL,
  `testcase_No` varchar(60) NOT NULL,
  `testCase_name` varchar(255) NOT NULL,
  `test_type` enum('Positive','Negative','Error/Exception Handling','Boundary Condition') NOT NULL,
  `status` enum('New','Active','Obsolete') NOT NULL,
  `test_result_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `testCase_name_UNIQUE` (`testCase_name`),
  KEY `executions_has_tes_cases_feature_id_foreign` (`feature_id`),
  KEY `test_cases_id` (`test_cases_id`),
  KEY `tep_id` (`tep_id`),
  KEY `executions_has_tes_cases_test_result_id_foreign` (`test_result_id`),
  KEY `executions_has_tes_cases_created_by_foreign` (`created_by`),
  KEY `executions_has_tes_cases_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `executions_has_tes_cases`
--

INSERT INTO `executions_has_tes_cases` (`id`, `project_id`, `tep_id`, `feature_id`, `test_cases_id`, `testcase_No`, `testCase_name`, `test_type`, `status`, `test_result_id`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, '', '1', 'Negative', 'Active', 1, 2, 2, 'A', '2020-06-10 15:09:29', '2020-06-10 15:09:29'),
(2, 1, 1, 1, 3, '', '2', 'Negative', 'Active', 1, 2, 2, 'A', '2020-06-10 15:09:29', '2020-06-10 15:09:29'),
(3, 1, 1, 1, 1, '', '3', 'Boundary Condition', 'New', 1, 2, 2, 'A', '2020-06-10 15:10:35', '2020-06-10 15:10:35'),
(4, 2, 3, 4, 7, '', '4', 'Negative', 'Active', 1, 2, 2, 'A', '2020-06-10 15:26:36', '2020-06-10 15:26:36');

-- --------------------------------------------------------

--
-- Table structure for table `executions_has_users`
--

DROP TABLE IF EXISTS `executions_has_users`;
CREATE TABLE IF NOT EXISTS `executions_has_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `tep_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `tep_id` (`tep_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `executions_has_users`
--

INSERT INTO `executions_has_users` (`id`, `user_id`, `tep_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, '2020-04-29 02:18:17', '2020-04-29 02:18:17'),
(2, NULL, 2, '2020-04-29 02:19:05', '2020-04-29 02:19:05'),
(3, NULL, 3, '2020-04-29 02:19:24', '2020-04-29 02:19:24'),
(4, NULL, 4, '2020-04-29 02:19:28', '2020-04-29 02:19:28'),
(5, NULL, 5, '2020-04-29 02:19:43', '2020-04-29 02:19:43');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
CREATE TABLE IF NOT EXISTS `features` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `feature_name` varchar(100) NOT NULL,
  `description` varchar(300) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL DEFAULT 'A',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `features_feature_name_unique` (`feature_name`),
  KEY `features_project_id_foreign` (`project_id`),
  KEY `features_created_by_foreign` (`created_by`),
  KEY `features_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `project_id`, `feature_name`, `description`, `status`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Feature ', 'Test add Feature', 'Active', 1, 1, 'A', '2020-04-29 00:03:07', '2020-05-04 08:29:05'),
(2, 1, 'Feature2', 'Test add Feature', 'Active', 2, 2, 'A', '2020-04-29 00:03:33', '2020-04-29 00:03:33'),
(3, 1, 'Feature3', 'Test add Feature', 'Active', 2, 2, 'A', '2020-04-29 00:03:43', '2020-04-29 00:03:43'),
(4, 2, '1.Feature', 'Test add Feature', 'Active', 2, 2, 'A', '2020-04-29 00:04:07', '2020-04-29 00:04:07'),
(5, 2, '2.Feature', 'Test add Feature', 'Active', 2, 2, 'A', '2020-04-29 00:04:11', '2020-04-29 00:04:11'),
(6, 3, 'A Feature', 'Test add Feature', 'Active', 2, 2, 'A', '2020-04-29 00:04:25', '2020-04-29 00:04:25'),
(7, 4, 'Feature ID4', 'Test add Feature 1', 'Active', 2, 2, 'A', '2020-04-30 09:23:21', '2020-04-30 09:23:21'),
(8, 4, 'Feature T1', 'Test add Feature', 'Active', 2, 2, 'A', '2020-05-03 13:34:38', '2020-05-03 13:34:38'),
(9, 4, 'Feature T2', 'Test add Feature', 'Active', 2, 2, 'A', '2020-05-04 08:25:28', '2020-05-04 08:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `feature_has_test_excutions`
--

DROP TABLE IF EXISTS `feature_has_test_excutions`;
CREATE TABLE IF NOT EXISTS `feature_has_test_excutions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` int(10) NOT NULL,
  `tep_id` int(10) UNSIGNED DEFAULT NULL,
  `feature_id` int(10) UNSIGNED DEFAULT NULL,
  `feature_name` varchar(100) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL DEFAULT 'A',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `feature_name_UNIQUE` (`feature_name`),
  KEY `feature_id` (`feature_id`),
  KEY `tep_id` (`tep_id`),
  KEY `feature_has_test_excutions_created_by_foreign` (`created_by`),
  KEY `feature_has_test_excutions_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feature_has_test_excutions`
--

INSERT INTO `feature_has_test_excutions` (`id`, `project_id`, `tep_id`, `feature_id`, `feature_name`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, 'a', 2, 2, 'A', '2020-04-29 19:24:44', '2020-04-29 19:40:15'),
(2, 1, 2, 2, 'b', 2, 2, 'A', '2020-04-29 19:24:44', '2020-04-29 19:40:15'),
(3, 1, 1, 1, 'c', 2, 2, 'D', '2020-04-29 19:24:44', '2020-06-05 16:53:42'),
(4, 1, 1, 1, 'd', 2, 2, 'D', '2020-04-29 19:24:44', '2020-06-05 16:53:50'),
(5, 1, 1, 1, 'e', 2, 2, 'D', '2020-04-29 19:24:44', '2020-06-05 17:20:59'),
(6, 4, 7, 7, 'f', 1, 1, 'A', '2020-04-30 09:33:52', '2020-04-30 09:33:52'),
(7, 1, 5, 3, 'g', 2, 2, 'A', '2020-05-03 20:30:37', '2020-05-03 20:30:37'),
(8, 1, 5, 3, 'h', 2, 2, 'A', '2020-05-03 20:33:30', '2020-05-03 20:33:30'),
(9, 1, 5, 1, 'i', 2, 2, 'A', '2020-05-03 20:34:09', '2020-05-03 20:34:09'),
(10, 1, 5, 3, 'j', 2, 2, 'A', '2020-05-03 20:34:09', '2020-05-03 20:34:09'),
(11, 1, 1, 1, 'k', 2, 2, 'A', '2020-05-04 08:58:27', '2020-06-05 17:26:04'),
(12, 1, 1, 3, 'l', 2, 2, 'A', '2020-05-19 17:29:52', '2020-05-19 17:29:52'),
(13, 1, 1, 5, 'm', 2, 2, 'A', '2020-05-19 17:29:52', '2020-05-19 17:29:52'),
(14, 1, 1, 1, 'n', 2, 2, 'A', '2020-06-04 16:08:53', '2020-06-05 17:26:09'),
(15, 1, 1, 1, 'o', 2, 2, 'A', '2020-06-05 15:02:41', '2020-06-05 16:29:10'),
(16, 3, 5, 6, 'p', 2, 2, 'A', '2020-06-08 16:12:55', '2020-06-08 16:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

DROP TABLE IF EXISTS `histories`;
CREATE TABLE IF NOT EXISTS `histories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `TephTc_id` int(10) UNSIGNED DEFAULT NULL,
  `test_result_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `histories_tephtc_id_foreign` (`TephTc_id`),
  KEY `histories_test_result_id_foreign` (`test_result_id`),
  KEY `histories_created_by_foreign` (`created_by`),
  KEY `histories_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `priority_settings`
--

DROP TABLE IF EXISTS `priority_settings`;
CREATE TABLE IF NOT EXISTS `priority_settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tep_id` int(10) UNSIGNED DEFAULT NULL,
  `priority_name` varchar(200) NOT NULL,
  `amount` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `priority_settings_priority_name_unique` (`priority_name`),
  KEY `priority_settings_tep_id_foreign` (`tep_id`),
  KEY `priority_settings_created_by_foreign` (`created_by`),
  KEY `priority_settings_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `priority_settings`
--

INSERT INTO `priority_settings` (`id`, `tep_id`, `priority_name`, `amount`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Immediate1', NULL, 2, 2, 'A', '2020-04-29 19:44:28', '2020-05-04 09:41:57'),
(2, NULL, 'Urgent', NULL, 1, 1, 'A', '2020-04-29 19:44:39', '2020-04-29 19:44:39'),
(3, NULL, 'High', NULL, 1, 1, 'A', '2020-04-29 19:44:55', '2020-04-29 19:44:55'),
(4, NULL, 'Normal', NULL, 1, 1, 'A', '2020-04-29 19:45:09', '2020-04-29 19:45:09'),
(5, NULL, 'Low', NULL, 1, 1, 'A', '2020-04-29 19:45:17', '2020-04-29 19:45:17'),
(6, NULL, 'Medium', NULL, 1, 1, 'A', '2020-05-04 09:29:59', '2020-05-04 09:29:59'),
(7, NULL, 'Low1', NULL, 1, 1, 'A', '2020-05-04 12:37:38', '2020-05-04 12:37:38');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` varchar(300) NOT NULL,
  `project_manager` varchar(100) NOT NULL,
  `business_analysis` varchar(100) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('A','D') NOT NULL DEFAULT 'A',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_project_name_unique` (`project_name`),
  KEY `projects_created_by_foreign` (`created_by`),
  KEY `projects_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `start_date`, `end_date`, `description`, `project_manager`, `business_analysis`, `created_by`, `modified_by`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Project1', '2018-04-20', '2018-10-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Inactive', 'A', '2020-04-28 23:53:04', '2020-05-04 08:20:34'),
(2, 'Project2', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'A', '2020-04-28 23:54:03', '2020-04-28 23:54:03'),
(3, 'Project3', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'A', '2020-04-28 23:54:15', '2020-04-28 23:54:15'),
(4, 'Project ID4', '2018-04-20', '2018-10-20', 'Test add Project', 'Mintra', 'Laura', 2, 2, 'Active', 'A', '2020-04-30 09:19:29', '2020-04-30 09:22:03'),
(5, 'Project4', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'A', '2020-05-03 08:52:39', '2020-05-03 08:52:39'),
(6, 'Project5', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'D', '2020-05-03 08:53:40', '2020-05-03 08:53:40'),
(7, 'Project6', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'D', '2020-05-03 08:56:42', '2020-05-03 08:56:42'),
(8, 'Project7', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'D', '2020-05-03 09:49:47', '2020-05-03 09:49:47'),
(9, 'Project8', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'A', '2020-05-03 10:22:20', '2020-05-03 10:22:20'),
(10, 'Project9', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'D', '2020-05-03 10:25:35', '2020-05-03 10:25:35'),
(11, 'Project10', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'D', '2020-05-03 10:28:25', '2020-05-03 10:28:25'),
(12, 'Project11', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'A', '2020-05-03 10:38:01', '2020-05-03 10:38:01'),
(13, 'Project T1', '2018-04-20', '2018-12-20', 'Test add Project', 'Sabena', 'Laura', 2, 2, 'Active', 'A', '2020-05-04 08:15:21', '2020-05-04 08:15:21');

-- --------------------------------------------------------

--
-- Table structure for table `servity_settings`
--

DROP TABLE IF EXISTS `servity_settings`;
CREATE TABLE IF NOT EXISTS `servity_settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tep_id` int(10) UNSIGNED DEFAULT NULL,
  `servity_name` varchar(200) NOT NULL,
  `amount` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `servity_settings_servity_name_unique` (`servity_name`),
  KEY `servity_settings_tep_id_foreign` (`tep_id`),
  KEY `servity_settings_created_by_foreign` (`created_by`),
  KEY `servity_settings_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `servity_settings`
--

INSERT INTO `servity_settings` (`id`, `tep_id`, `servity_name`, `amount`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Critical1', NULL, 2, 2, 'A', '2020-04-29 19:49:06', '2020-05-04 09:46:55'),
(2, NULL, 'Major', NULL, 2, 2, 'A', '2020-04-29 19:49:15', '2020-04-29 19:49:15'),
(3, NULL, 'Minor', NULL, 2, 2, 'A', '2020-04-29 19:49:22', '2020-04-29 19:49:22'),
(4, NULL, 'Trivial', NULL, 2, 2, 'A', '2020-04-29 19:49:30', '2020-04-29 19:49:30'),
(5, NULL, 'soft', NULL, 2, 2, 'A', '2020-05-04 09:44:26', '2020-05-04 09:44:26'),
(6, NULL, 'Trivial1', NULL, 2, 2, 'A', '2020-05-04 12:38:54', '2020-05-04 12:38:54');

-- --------------------------------------------------------

--
-- Table structure for table `test_cases`
--

DROP TABLE IF EXISTS `test_cases`;
CREATE TABLE IF NOT EXISTS `test_cases` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `feature_id` int(10) UNSIGNED DEFAULT NULL,
  `testcase_No` varchar(60) NOT NULL,
  `testCase_name` varchar(255) NOT NULL,
  `test_type` enum('Positive','Negative','Error/Exception Handling','Boundary Condition') NOT NULL,
  `status` enum('New','Active','Obsolete') NOT NULL,
  `priority` enum('High','Medium','Low') NOT NULL,
  `test_by` enum('Manual','Automate') NOT NULL,
  `objective` varchar(600) NOT NULL,
  `precondition` varchar(600) NOT NULL,
  `test_steps` varchar(2000) NOT NULL,
  `expected_results` varchar(2000) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL DEFAULT 'A',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `testCase_name_UNIQUE` (`testCase_name`),
  KEY `test_cases_project_id_foreign` (`project_id`),
  KEY `test_cases_feature_id_foreign` (`feature_id`),
  KEY `test_cases_created_by_foreign` (`created_by`),
  KEY `test_cases_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_cases`
--

INSERT INTO `test_cases` (`id`, `project_id`, `feature_id`, `testcase_No`, `testCase_name`, `test_type`, `status`, `priority`, `test_by`, `objective`, `precondition`, `test_steps`, `expected_results`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'AD_TC_001 T1.1', 'Add Test Case T1.1', 'Boundary Condition', 'New', 'High', 'Manual', 'Susa', 'To eliminate user who don\'t have permission', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login', 2, 2, 'A', '2020-04-29 01:39:50', '2020-05-04 10:53:05'),
(11, 3, 6, 'AD_TC_001 T3', 'Add Test Case T3', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-05-04 08:30:37', '2020-05-04 08:30:37'),
(2, 1, 1, 'AD_TGE_002', 'Add Test Case in Feature under Project 2.0', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-29 01:40:01', '2020-04-29 01:40:01'),
(3, 1, 1, 'AD_TGE_003', 'Add Test Case in Feature under Project 3.0', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-29 01:40:08', '2020-04-29 01:40:08'),
(4, 1, 2, 'AD_TTE_001', 'Add Test Case in Feature under Project T1.0', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-29 01:41:00', '2020-04-29 01:41:00'),
(5, 1, 2, 'AD_TTE_002', 'Add Test Case in Feature under Project T2.0', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-29 01:41:07', '2020-04-29 01:41:07'),
(6, 1, 3, 'AD_THE_001', 'Add Test Case in Feature under Project H1.0', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-29 01:41:44', '2020-04-29 01:41:44'),
(7, 2, 4, 'AD_TC_001', 'Add Test Case in Feature under Project TC 1.0', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-29 01:42:09', '2020-04-29 01:42:09'),
(8, 3, 6, 'AD_TFC_001', 'Add Test Case in Feature under Project TFC 1.0', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-29 01:42:45', '2020-04-29 01:42:45'),
(9, 4, 7, 'AD_MM 10', 'Add Test Case in Feature under Project TFC 10', 'Negative', 'Active', 'High', 'Automate', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-04-30 09:25:36', '2020-04-30 09:25:36'),
(10, 3, 6, 'AD_TC_001 T1', 'Add Test Case T1', 'Negative', 'Active', 'High', 'Manual', 'To eliminate user who don\'t have permission', 'No pre-condition for this case', '1. Navigate to login page\n2. Provide valid username\n3. Procide invalid password\n4. Click login button', '1. System display error message \"\"Cannot login due to invalid username or password\"\"\n2. System not allow to login\n', 2, 2, 'A', '2020-05-03 15:01:26', '2020-05-03 15:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `test_execution_plans`
--

DROP TABLE IF EXISTS `test_execution_plans`;
CREATE TABLE IF NOT EXISTS `test_execution_plans` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `tep_name` varchar(300) NOT NULL,
  `description` varchar(300) NOT NULL,
  `priority` enum('A','High','Medium','Low') NOT NULL,
  `test_type` enum('A','Positive','Negative','Error/Exception Handling','Boundary Condition') NOT NULL,
  `test_by` enum('A','Manual','Automate') NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('A','D') NOT NULL DEFAULT 'A',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `test_execution_plans_tep_name_unique` (`tep_name`),
  KEY `test_execution_plans_project_id_foreign` (`project_id`),
  KEY `test_execution_plans_created_by_foreign` (`created_by`),
  KEY `test_execution_plans_modified_by_foreign` (`modified_by`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_execution_plans`
--

INSERT INTO `test_execution_plans` (`id`, `project_id`, `tep_name`, `description`, `priority`, `test_type`, `test_by`, `created_by`, `modified_by`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Add TEP', 'Test add Test Execution Plan', 'High', 'Error/Exception Handling', 'Automate', 2, 2, 'Active', 'A', '2020-04-29 02:18:17', '2020-05-04 08:56:32'),
(2, 1, 'Add TEP 2', 'Test add Test Execution Plan', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-04-29 02:19:05', '2020-04-29 02:19:05'),
(3, 2, '1. Add TEP ', 'Test add Test Execution Plan', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-04-29 02:19:24', '2020-04-29 02:19:24'),
(4, 2, '2. Add TEP ', 'Test add Test Execution Plan', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-04-29 02:19:28', '2020-04-29 02:19:28'),
(5, 3, 'Add TEP  Test', 'Test add Test Execution Plan', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-04-29 02:19:43', '2020-04-29 02:19:43'),
(6, 1, 'Add TEP 2.0', 'Test add Test Execution Plan 2.0', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-04-29 09:25:18', '2020-04-29 09:25:18'),
(7, 1, 'Add TEP Min', 'Test add Test Execution Plan Min', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-04-30 09:29:13', '2020-04-30 09:29:13'),
(8, 1, 'Add TEP 3.0', 'Test add Test Execution Plan 3.0', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-05-03 19:20:40', '2020-05-03 19:20:40'),
(9, 1, 'Add TEP 4.0', 'Test add Test Execution Plan 4.0', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-05-04 08:54:02', '2020-05-04 08:54:02'),
(10, 1, 'Add TEP 4.1', 'Test add Test Execution Plan 4.1', 'High', 'Positive', 'Manual', 2, 2, 'Active', 'A', '2020-05-04 11:02:51', '2020-05-04 11:02:51');

-- --------------------------------------------------------

--
-- Table structure for table `test_results`
--

DROP TABLE IF EXISTS `test_results`;
CREATE TABLE IF NOT EXISTS `test_results` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `deleted` enum('Active','Delete') NOT NULL DEFAULT 'Active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `status_UNIQUE` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_results`
--

INSERT INTO `test_results` (`id`, `name`, `created_by`, `modified_by`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Unattempted', 2, 2, 'Active', '2019-12-16 17:33:37', '2020-06-10 12:41:08'),
(2, 'Passed', 2, 2, 'Active', '2019-12-16 17:36:03', '2019-12-16 17:36:03'),
(3, 'Passed_Retest', 2, 2, 'Active', '2019-12-16 17:36:25', '2019-12-16 17:36:25'),
(4, 'Failed', 2, 2, 'Active', '2019-12-16 17:36:42', '2019-12-16 17:36:42'),
(5, 'Failed_Retest', 2, 2, 'Active', '2019-12-16 17:36:55', '2019-12-16 17:36:55'),
(6, 'Block_Dependency', 2, 2, 'Active', '2019-12-16 17:37:29', '2019-12-16 17:37:29'),
(7, 'Block_No Code Drop', 2, 2, 'Active', '2019-12-16 17:37:47', '2019-12-16 17:37:47');

-- --------------------------------------------------------

--
-- Table structure for table `timestamps`
--

DROP TABLE IF EXISTS `timestamps`;
CREATE TABLE IF NOT EXISTS `timestamps` (
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `token_created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `type` varchar(80) NOT NULL,
  `is_revoked` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unique` (`token`),
  KEY `tokens_user_id_foreign` (`user_id`),
  KEY `tokens_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(600) DEFAULT NULL,
  `username` varchar(80) NOT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(60) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `role` enum('Admin','User') NOT NULL,
  `status_user` enum('A','I') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_token_unique` (`token`),
  KEY `users_token_index` (`token`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `image`, `username`, `email`, `password`, `firstname`, `lastname`, `role`, `status_user`, `created_at`, `updated_at`, `token`, `token_created_at`) VALUES
(1, NULL, 'Anna', 'Anna@gmail.com', '$2a$10$LVIEFpsZkyXx0w3r2Elln.7bI/GKdIVS6VpHLLgKpWGAsTTaZ6KOO', 'Anna', 'kendrick', 'Admin', 'A', '2020-04-28 23:03:53', '2020-04-28 23:03:53', NULL, NULL),
(2, NULL, 'Mark', 'Mark@gmail.com', '$2a$10$tQioq/3DeSDMENbH96dj/OurTDu7mlPOd52vJaITeP4eiZE6xEAk2', 'Prin', 'Suparat', 'User', 'A', '2020-04-28 23:12:13', '2020-05-26 21:46:35', '6bfcc4e0edb8d9829ddd', '2020-05-26 14:46:36'),
(3, NULL, 'Mai', 'Mai@gmail.com', '$2a$10$4SZvkXXs.Dby/Jp3Hpwg9eF6xPLbtpT2XDXvjDsyuYEXFpqXNtug6', 'Davika', 'Hoorne', 'User', 'A', '2020-04-28 23:18:11', '2020-04-28 23:18:11', NULL, NULL),
(4, NULL, 'Mintra', 'Mintra@gmail.com', '$2a$10$yztFBqhKJwRN/BAX9ht1VeNCg.Mojwqt/c3rQ/iCEEvOzQihQtfum', 'Mintra', 'Panyana', 'Admin', 'A', '2020-04-30 09:17:46', '2020-04-30 09:17:46', NULL, NULL),
(5, NULL, 'Laura', 'Laura@gmail.com', '$2a$10$2tqj.OjGYumcaRl6JKZkMes5wmctjiuEZYOqT0/juIHA4tJ9CM2m2', 'Laura', 'Tesoro', 'User', 'A', '2020-05-02 22:41:39', '2020-05-02 22:41:39', NULL, NULL),
(7, NULL, 'Laurana', 'Laurana@gmail.com', '$2a$10$2pLKAkrrP2p7tOk35yDeR.RKMOq136ZhG3.jRvazQknLOvtvCp/XW', 'Laura', 'Tesoro', 'User', 'A', '2020-05-04 08:00:41', '2020-05-04 08:00:41', NULL, NULL),
(6, NULL, 'Nana', 'Nana@gmail.com', '$2a$10$y8YcofUKH5pDj98WSjL8AOmWUOS569RDlmZI2IyNcUoA5lvfud4Eq', 'Davika', 'Hoorne', 'User', 'A', '2020-05-03 02:51:18', '2020-05-03 02:51:18', NULL, NULL),
(8, NULL, 'Nana1', 'Nana1@gmail.com', '$2a$10$7tQayUxdfPh09.fDZwN/n.Ddv8AGhQGjUoM.ylI9y/Lz/lk5Fi3uq', 'Nana', 'Hana', 'User', 'A', '2020-05-04 08:04:28', '2020-05-04 08:04:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_has_projects`
--

DROP TABLE IF EXISTS `users_has_projects`;
CREATE TABLE IF NOT EXISTS `users_has_projects` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted` enum('A','D') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `project_id` (`project_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_has_projects`
--

INSERT INTO `users_has_projects` (`id`, `user_id`, `project_id`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'A', '2020-04-28 23:53:04', '2020-04-29 01:33:36'),
(2, 2, 1, 'A', '2020-04-28 23:53:04', '2020-04-28 23:53:04'),
(3, 1, 2, 'A', '2020-04-28 23:54:03', '2020-04-28 23:54:03'),
(4, 3, 2, 'A', '2020-04-28 23:54:03', '2020-04-28 23:54:03'),
(5, 2, 3, 'A', '2020-04-28 23:54:16', '2020-04-28 23:54:16'),
(6, 3, 3, 'A', '2020-04-28 23:54:16', '2020-04-28 23:54:16'),
(7, 3, 1, 'D', '2020-04-29 00:01:03', '2020-04-29 01:31:45'),
(8, 2, 4, 'A', '2020-04-30 09:19:29', '2020-04-30 09:19:29'),
(9, 3, 4, 'A', '2020-04-30 09:19:30', '2020-04-30 09:19:30'),
(10, 2, 5, 'A', '2020-05-03 08:52:39', '2020-05-03 08:52:39'),
(11, 2, 9, 'A', '2020-05-03 10:22:20', '2020-05-03 10:22:20'),
(12, 1, 9, 'A', '2020-05-03 10:22:20', '2020-05-03 10:22:20'),
(13, 2, 11, 'D', '2020-05-03 10:28:25', '2020-05-04 08:22:59'),
(14, 2, 12, 'A', '2020-05-03 10:38:01', '2020-05-03 10:38:01'),
(15, 2, 13, 'A', '2020-05-04 08:15:21', '2020-05-04 08:15:21'),
(16, 3, 11, 'A', '2020-05-04 08:23:21', '2020-05-04 08:23:21');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
