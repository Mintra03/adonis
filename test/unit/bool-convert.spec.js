'use strict'

const { bool_conv_db } = use('App/Helpers/BoolConvert')

const { test } = use('Test/Suite')('Helpers : Bool Convert')

test('bool_conv_db() int input', async ({ assert }) => {
  assert.isTrue(bool_conv_db(1))
  assert.isFalse(bool_conv_db(0))
})

test('bool_conv_db() int bad input', async ({ assert }) => {
  assert.isNull(bool_conv_db(34234235))
})

test('bool_conv_db() string input', async ({ assert }) => {
  assert.isTrue(bool_conv_db('true'))
  assert.isFalse(bool_conv_db('false'))
  assert.isTrue(bool_conv_db("true"))
  assert.isFalse(bool_conv_db("false"))
})

test('bool_conv_db() string bad input', async ({ assert }) => {
  assert.isNull(bool_conv_db('ghjgjh'))
  assert.isNull(bool_conv_db(null))
})

test('bool_conv_db() boolean input', async ({ assert }) => {
  assert.isTrue(bool_conv_db(true))
  assert.isFalse(bool_conv_db(false))
})
