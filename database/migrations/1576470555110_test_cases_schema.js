'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TestCasesSchema extends Schema {
  up () {
    this.create('test_cases', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('feature_id').unsigned().references('id').inTable('features')
      table.string('testcase_No', 60).notNullable()
      table.string('testCase_name', 255).notNullable().unique()
      table.enum('test_type', ['Positive', 'Negative', 'Error/Exception Handling', 'Boundary Condition']).notNullable()
      table.enum('status', ['New', 'Active', 'Obsolete']).notNullable()
      table.enum('priority', ['High', 'Medium', 'Low']).notNullable()
      table.enum('test_by' , ['Manual', 'Automate']).notNullable()
      table.string('objective', 600).notNullable()
      table.string('precondition', 600).notNullable()
      table.string('test_steps', 2000).notNullable()
      table.string('expected_results', 2000).notNullable()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('test_cases')
  }
}

module.exports = TestCasesSchema
