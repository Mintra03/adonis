'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.create('projects', (table) => {
      table.increments()
      table.string('project_name', 100).notNullable().unique()
      table.date('start_date')
      table.date('end_date')
      table.string('description', 300).notNullable()
      table.string('project_manager', 100).notNullable()
      table.string('business_analysis', 100).notNullable()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('status', ['Active', 'Inactive']).notNullable()
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('projects')
  }
}

module.exports = ProjectSchema
