'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExecutionsHasTesCasesSchema extends Schema {
  up () {
    this.create('executions_has_tes_cases', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('feature_id').unsigned().references('id').inTable('features')
      table.integer('test_cases_id').unsigned().index('test_cases_id')
      table.integer('tep_id').unsigned().index('tep_id')
      table.foreign('test_cases_id').references('id').inTable('test_cases')
      table.string('testcase_No', 60).notNullable()
      table.string('testCase_name', 255).notNullable().unique()
      table.foreign('tep_id').references('id').inTable('test_execution_plans')
      table.integer('test_result_id').unsigned().references('id').inTable('test_results')
      table.enum('test_type', ['Positive', 'Negative', 'Error/Exception Handling', 'Boundary Condition']).notNullable()
      table.enum('status', ['New', 'Active', 'Obsolete']).notNullable()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('executions_has_tes_cases')
  }
}

module.exports = ExecutionsHasTesCasesSchema
