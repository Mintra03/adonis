'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExecutionsHasUsersSchema extends Schema {
  up () {
    this.create('executions_has_users', (table) => {
      table.increments()
      table.integer('user_id').unsigned().index('user_id')
      table.integer('tep_id').unsigned().index('tep_id')
      table.foreign('user_id').references('id').inTable('users')
      table.foreign('tep_id').references('id').inTable('test_execution_plans')
      table.timestamps()
    })
  }

  down () {
    this.drop('executions_has_users')
  }
}

module.exports = ExecutionsHasUsersSchema
