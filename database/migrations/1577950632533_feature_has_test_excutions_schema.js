'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FeatureHasTestExcutionSchema extends Schema {
  up () {
    this.create('feature_has_test_excutions', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.integer('feature_id').unsigned().references('id').inTable('features')
      table.integer('tep_id').unsigned().index('tep_id')
      // table.foreign('feature_id').references('id').inTable('features')
      table.string('feature_name', 100).notNullable().unique()
      table.foreign('tep_id').references('id').inTable('test_execution_plans')
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('feature_has_test_excutions')
  }
}

module.exports = FeatureHasTestExcutionSchema
