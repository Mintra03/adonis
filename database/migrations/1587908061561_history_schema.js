'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HistorySchema extends Schema {
  up () {
    this.create('histories', (table) => {
      table.increments()
      table.integer('TephTc_id').unsigned().references('id').inTable('executions_has_tes_cases')
      table.integer('test_result_id').unsigned().references('id').inTable('test_results')
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('histories')
  }
}

module.exports = HistorySchema
