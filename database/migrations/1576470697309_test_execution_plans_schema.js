'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TestExecutionPlansSchema extends Schema {
  up () {
    this.create('test_execution_plans', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.string('tep_name', 300).notNullable().unique()
      table.string('description', 300).notNullable()
      table.enum('priority', ['A','High', 'Medium', 'Low']).notNullable()
      table.enum('test_type', ['A','Positive', 'Negative', 'Error/Exception Handling', 'Boundary Condition']).notNullable()
      table.enum('test_by' , ['A','Manual', 'Automate']).notNullable()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('status', ['Active', 'Inactive']).notNullable()
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('test_execution_plans')
  }
}

module.exports = TestExecutionPlansSchema
