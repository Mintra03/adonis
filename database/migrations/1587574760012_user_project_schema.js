'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserProjectSchema extends Schema {
  up () {
    this.create('users_has_projects', (table) => {
      table.increments()
      table.integer('user_id').unsigned().index('user_id')
      table.integer('project_id').unsigned().index('project_id')
      table.foreign('user_id').references('id').inTable('users')
      table.foreign('project_id').references('id').inTable('projects')
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users_has_projects')
  }
}

module.exports = UserProjectSchema
