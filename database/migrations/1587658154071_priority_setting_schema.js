'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PrioritySettingSchema extends Schema {
  up () {
    this.create('priority_settings', (table) => {
      table.increments()
      table.integer('tep_id').unsigned().references('id').inTable('test_execution_plans')
      table.string('priority_name', 200).notNullable().unique()
      table.integer('amount').unsigned()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('priority_settings')
  }
}

module.exports = PrioritySettingSchema
