'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TestResultSchema extends Schema {
  up () {
    this.create('test_results', (table) => {
      table.increments()
      table.string('name', 60).notNullable().unique()
      table.enum('deleted', ['Active', 'Delete']).notNullable()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('test_results')
  }
}

module.exports = TestResultSchema
