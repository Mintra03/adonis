'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('image', 600).nullable();
      table.string('username', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('firstname', 100).notNullable()
      table.string('lastname', 100).notNullable()
      table.enum('role', ['Admin', 'User']).notNullable()
      table.enum('status_user', ['A','I']).notNullable()
      // table.enum('delete', ['A','D']).notNullable()
      table.timestamps()
      table.string('token', 255).unique().index()
      table.timestamp('token_created_at') // date when token was created
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
