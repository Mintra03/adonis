'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DefectStatusSettingSchema extends Schema {
  up () {
    this.create('defect_status_settings', (table) => {
      table.increments()
      table.integer('tep_id').unsigned().references('id').inTable('test_execution_plans')
      table.string('defect_status_name', 200).notNullable().unique()
      table.integer('amount').unsigned()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('defect_status_settings')
  }
}

module.exports = DefectStatusSettingSchema
