'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FeatureSchema extends Schema {
  up () {
    this.create('features', (table) => {
      table.increments()
      table.integer('project_id').unsigned().references('id').inTable('projects')
      table.string('feature_name', 100).notNullable().unique()
      table.string('description', 300).notNullable()
      table.enum('status', ['Active', 'Inactive']).notNullable()
      table.integer('created_by').unsigned().references('id').inTable('users')
      table.integer('modified_by').unsigned().references('id').inTable('users')
      table.enum('deleted', ['A', 'D']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('features')
  }
}

module.exports = FeatureSchema
