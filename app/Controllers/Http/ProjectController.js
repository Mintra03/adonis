'use strict'
const Project = use('App/Models/Project');
const User = use('App/Models/User')
const Users_has_projects = use('App/Models/Users_has_Projects');
const Mail = use('Mail');
const moment = require('moment')
const crypto = require('crypto') 
const Database = use('Database')
const Feature = use('App/Models/Feature')
const TestCase = use('App/Models/TestCase')

class ProjectController {

    async projects({ auth, response}) {
        try {
            const project = await Database.from('projects')
                                        .innerJoin('users', 'projects.modified_by','users.id')
                                        .select('projects.id','projects.project_name','projects.start_date',
                                        'projects.end_date','projects.description','projects.project_manager','projects.business_analysis',
                                        'projects.created_by','projects.modified_by','users.username','projects.status','projects.deleted',
                                        'projects.created_at','projects.updated_at')
                                        .where('projects.deleted', 'A')
                                        .whereNot('users.status_user', 'D')
            
            return response.json({ "projects":project })
          }
          catch (e) {
            return response.status(403).send({message: { error:'For admin only!'}})
          }
    }

    async project({ auth, response}) {
        try {
            const project = await Database.from('projects')
                                            .innerJoin('users_has_projects','users_has_projects.project_id','projects.id')
                                            .innerJoin('users','users_has_projects.user_id','users.id')
                                            .innerJoin('users', 'projects.modified_by','users.id')
                                            .select('projects.id','projects.project_name','projects.start_date',
                                            'projects.end_date','projects.description','projects.project_manager','projects.business_analysis',
                                            'projects.created_by','projects.modified_by','users.username','projects.status','projects.deleted',
                                            'projects.created_at','projects.updated_at')
                                            .where('projects.deleted', 'A')
                                            .whereNot('users_has_projects.deleted', 'D')
                                            .whereNot('users.status_user', 'D')
                                            .where('users_has_projects.user_id', auth.user.id)
                                            .orWhere('users.role','Admin')
                                            .groupBy('projects.id')
            
            return response.json({ "projects": project })
          }
          catch (e) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }

    async create({request,auth,response}) {
        try {
            const projects = []
            projects.project_name = request.input("project_name")
            projects.start_date = request.input("start_date")
            projects.end_date = request.input("end_date")
            projects.description = request.input("description")
            projects.project_manager = request.input("project_manager")
            projects.business_analysis = request.input("business_analysis")
            projects.status = request.input("status")

            const userIds = [...(request.input("user") || [])]
            const users = await User.getUsers(userIds)
            
            if (!users[0]) {
                return response.status(404).send({message: { error:'Resource not found user.'} })
            } 
                let project = new Project()
                const user = await project.saveUser({users,projects,auth})
                // return response.json({message: 'Your create Project finish.'})
                return response.json({ "projects":project,"users":user })
        }
        catch (e) {
            return response.status(400).send({message: { error:'Your create Project fail!'} })
        }
    }

    async update({request, params, auth, response}) {
        // console.log(params);
            const { id } = params
            const project = await Project.find(id)
            
            try {
                if (project) {
                    project.created_by = await auth.user.id
                    project.modified_by = await auth.user.id
                    project.project_name = request.input("project_name")
                    project.start_date = request.input("start_date")
                    project.end_date = request.input("end_date")
                    project.description = request.input("description")
                    project.project_manager = request.input("project_manager")
                    project.business_analysis = request.input("business_analysis")
                    project.status = request.input("status")

                    await project.save()
                    return response.json({message: 'Your update Project finish.' })
                }
                return response.status(404).send({message: { error:'Resource not found.'} })
            }
            catch (e) {
                console.log(e);
                return response.status(400).send({message: { error:'Bad Request!'} })
            }
    }

    async addUserinProject({request,auth,response,params}) {
        const project = await Project.findOrFail(params.id);

        const user_id = request.input("user_id")

        let member = new Users_has_projects()
        member.user_id = user_id
        member.project_id = project.id

        try { 
            if (!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project) {
                    if (project.deleted == 'A') {
                    const sheckUser = await Database.count('* as countUser')
                                                    .from('users_has_projects')
                                                    .innerJoin('projects', 'users_has_projects.project_id', 'projects.id') 
                                                    .select('users_has_projects.project_id','users_has_projects.user_id')
                                                    .where('projects.deleted', 'A')
                                                    .where('users_has_projects.deleted', 'A')
                                                    .where('users_has_projects.project_id', project.id)
                                                    .where('users_has_projects.user_id', member.user_id)

                        const sheck_member = sheckUser[0].countUser
                        if (sheck_member == 0) {
                            member = await member.save()
                            return response.json({message: 'Your add User in Project finish.'})
                        }
                    }
                    return response.json({message: 'Your add User in Project fail or User is already in this project.'})
                }
                return response.status(400).send({message: { error:'Project is deleted!'} })
            } catch (error) {
                return response.status(400).send({message: { error:'Bad Request!'} })
            }
    }

    async viewproject ({ params, response ,auth}) {
        const project = await Project.findOrFail(params.id);
            try {
                if(!project) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                }
                if(project) {
                    const feature = await project
                                .features()
                                .fetch()

                    const testCase = await project
                                    .testCases()
                                    .fetch()

                // ถ้ามีการอนุญาติ user แล้ว ต้องแก้สิทธิ์เข้าถึง Project
                    return response.json({ "project": project, "feature": feature, "testCase": testCase });
                }
            } catch (error) {
                return response.status(403).send({message: { error:'Sorry, you do not have permission to access.'}})
            }
    }

    async viewUserProject({params,response}){
        const { id } = params
        const project = await Project.find(id)
        try {
            if (!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project.deleted == 'A') {
                // const user = await project
                //             .usersProjects()
                //             .fetch()
                            
            const user = await Database.table('users_has_projects')
                                        .innerJoin('projects', 'users_has_projects.project_id', 'projects.id')
                                        .innerJoin('users', 'projects.modified_by','users.id')
                                        .select('users_has_projects.user_id','users.username')
                                        .where('users_has_projects.deleted' , 'A')
                                        .whereNot('projects.deleted' , 'D')
                                        .where('users_has_projects.project_id', project.id)
                            
                return response.json({ "project": project, "user": user})
            }
            return response.json({ "Project": project, message: 'Project is deleted!'})
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }        
    }

    async viewFeatureProject({params,response}){
        const { id } = params
        const project = await Project.find(id)
        try {
            if (!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project.deleted == 'A') {
            // const feature = await project
            //                 .features()
            //                 .fetch()
                
            const feature = await Database.table('features')
                                            .innerJoin('projects', 'projects.id', 'features.project_id')
                                            .innerJoin('users', 'features.modified_by','users.id')
                                            .select('features.id','features.feature_name','features.created_by','users.username',
                                            'features.modified_by','features.created_at','features.updated_at')
                                            .where('features.deleted' , 'A')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('users.status_user', 'D')
                                            .where('features.project_id', project.id)
                
                return response.json({ "project": project, "feature": feature})
                }
            return response.json({ message: 'Project is deleted!'})
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async viewTestCaseProject({params,response}){
        const project = await Project.findOrFail(params.id)
        const feature = await Feature.findOrFail(params.idft)
        try {
            if (!project) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project.deleted == 'A') {
            // const listfeature = await project
            //                 .features()
            //                 .fetch()
            // const listtestCase = await project
            //                 .testCases()
            //                 .fetch()

            // const listfeature = await Database.table('features')
            //                                 .innerJoin('projects', 'projects.id', 'features.project_id')
            //                                 .select('features.id')
            //                                 .where('features.deleted' , 'A')
            //                                 .whereNot('projects.deleted' , 'D')
            //                                 .where('features.project_id', project.id)

                // if (listfeature == feature) {
                const testCase = await Database.table('test_cases')
                                            .innerJoin('projects', 'test_cases.project_id', 'projects.id')
                                            .innerJoin('features', 'test_cases.feature_id', 'features.id')
                                            .innerJoin('users', 'test_cases.modified_by','users.id')
                                            .select('test_cases.id','test_cases.testcase_No','test_cases.testCase_name','test_cases.test_type',
                                            'test_cases.status','test_cases.priority','test_cases.test_by',
                                            'test_cases.objective','test_cases.precondition','test_cases.test_steps',
                                            'test_cases.expected_results','test_cases.created_by','test_cases.modified_by','users.username',
                                            'test_cases.created_at','test_cases.updated_at')
                                            .where('test_cases.deleted' , 'A')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('features.deleted', 'D')
                                            .whereNot('users.status_user', 'D')
                                            .where('test_cases.project_id', project.id)
                                            .where('test_cases.feature_id', feature.id)
                    
                    return response.json({ "project": project, "feature": feature, "testCase": testCase})
                // }
                // return response.status(404).send({message: { error:'Resource not found.'} })
            }
            return response.status(404).send({message: { error:'Resource not found.'} })
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async viewAll({params,response}){
        const { id } = params
        const project = await Project.find(id)
        const feature = await Feature.findOrFail(params.idft)
        const testcase = await TestCase.findOrFail(params.idtc)
        try {
            if (!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project.deleted == 'A') {

            // const feature = await project
            //                 .features()
            //                 .fetch()
            // const testCase = await project
            //                 .testCases()
            //                 .fetch()
                
            const showAll = await Database.table('test_cases')
                                            .innerJoin('projects', 'test_cases.project_id', 'projects.id')
                                            .innerJoin('features', 'test_cases.feature_id', 'features.id')
                                            .innerJoin('users', 'test_cases.modified_by','users.id')
                                            .select('test_cases.id','test_cases.testcase_No','test_cases.testCase_name','test_cases.test_type',
                                            'test_cases.status','test_cases.priority','test_cases.test_by',
                                            'test_cases.objective','test_cases.precondition','test_cases.test_steps',
                                            'test_cases.expected_results','test_cases.created_by','test_cases.modified_by','users.username',
                                            'test_cases.created_at','test_cases.updated_at')
                                            .where('test_cases.deleted' , 'A')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('features.deleted', 'D')
                                            .whereNot('users.status_user', 'D')
                                            .where('test_cases.project_id', project.id)
                                            .where('test_cases.feature_id', feature.id)
                                            .where('test_cases.id', testcase.id)
                
                return response.json({ "Project": project, "Feature": feature, "testCase": showAll})
                }
            return response.json({ message: 'Project is deleted!'})
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async changestatus ({ params, request , response }) {
      const { id } = params
      const project = await Project.find(id)
      var status = request.input("status")
     
      try { 
          if (!project) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          } else if (project) {
          
            if(project.status == status){
              return response.json({message: "Current project status"+" "+ project.project_name +" : "+ project.status})
            }

            project.status = status
            await project.save()
            return response.json({message: "Your Change Project Status" +" "+ project.project_name +" : "+ project.status })
          }
          return response.status(400).send({message: { error:'Your Change status Project fail, Please your ckeck status Project!'} })
      } catch (error) {
        return response.status(400).send({message: { error:'Bad Request!'} })
      }
    }

    async delete({ params, response }) {
            const { id } = params
            const project = await Project.find(id)
            try { 
                const feature = await project
                            .features()
                            .fetch()

                const testCase = await project
                            .testCases()
                            .fetch()

                if (!project) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (project) {
                if ( project.deleted == 'D' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ project.project_name +" "+ project.deleted +":Deleted"})
                }

                project.deleted = 'D'
                await project.save()
                return response.json({message: "Your Deleted Status" +" "+ project.project_name +" "+ project.deleted +":Deleted"})
                }
            
            // await user.delete()
            return response.status(404).send({message: { error:'Your deleted Project fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async undeleted ({ params, response }) {
        const { id } = params
        const project = await Project.find(id)
        try { 
            const feature = await project
                            .features()
                            .fetch()

            const testCase = await project
                            .testCases()
                            .fetch()

            if (!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project) {
            if ( project.deleted == 'A' ) {
                return response.json({ message: "Current Deleted Status" +" "+ project.project_name +" "+ project.deleted +":Active"})
            }

            project.deleted = 'A'
            await project.save()
            return response.json({message: "Your Change Deleted Status" +" "+ project.project_name +" "+ project.deleted +":Active"})
            }
            return response.status(404).send({message: { error:'Your Change Deleted status Project fail!'} })
        } catch (error) {
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
    }

    async deleteUserProject({params,response}){
        const { id } = params
        const project = await Project.find(id)
        const user = await Users_has_projects.findOrFail(params.iduser)

        // const feature = await project
        //                     .features()
        //                     .fetch()

        try {
            if (!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project) {
            if (user.deleted == 'D') {
                return response.json({message: "Current Deleted Status" +" "+ user.user_id +" "+ user.deleted +":Deleted"})
            }
            const showUser = await Database.table('users_has_projects')
                                        .innerJoin('projects', 'users_has_projects.project_id', 'projects.id')
                                        .select('users_has_projects.project_id','users_has_projects.user_id')
                                        .where('users_has_projects.deleted' , 'A')
                                        .whereNot('projects.deleted' , 'D')
                                        .where('users_has_projects.project_id', project.id)
                                        .where('users_has_projects.user_id', user.id)

                    const userId = await Users_has_projects.findBy(showUser[0])
                    userId.deleted = 'D'
                    await userId.save()
                    return response.json({message: "Your Deleted Status" +" "+ userId.id +" "+ userId.deleted +":Deleted"})
                }
            return response.status(404).send({message: { error:'Your Deleted User in Project fail!'} })
        } catch (e) { 
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
    }

    // async sendemail ({ request, response }) {
    //     try {
    //         const {email} = request.only(['email'])
    //         const project_name = request.input("project_name")

    //         const user = await User.findByOrFail('email',email)
    //         const token = await crypto.randomBytes(10).toString('hex')
    //         user.token = token
           
    //         await Mail.send('sendemail', { user, token, project_name }, (message) => {
    //                 message
    //             .from('myemail@.com')
    //             .to(email)
    //             .subject('Welcome to Project')
    //         })
    //         const useremail = await project.sendemail({users,projects,project_name , email})
    //         return response.json({useremail});
    //         // response.json({message: 'You send Email successfully.' })
    //     } catch (error) {
    //         return response.json({ message: 'Your send email to invite join Project fail.' });
    //     }
    // }

}

module.exports = ProjectController
