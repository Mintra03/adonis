'use strict'
const TestExecutionPlan = use('App/Models/TestExecutionPlan');
const FeatureHasTestExcution = use('App/Models/Feature_has_Test_excution')
const Feature = use('App/Models/Feature')
const TestResult = use('App/Models/TestResult')
const ExecutionsHasTesCase = use('App/Models/ExecutionsHasTesCase')
const TestCase = use('App/Models/TestCase')
const User = use('App/Models/User')
const Database = use('Database')
const History = use('App/Models/History')
const Project = use('App/Models/Project')

class TepController {

    async teps({ auth, response, params}) {
        const project = await Project.findOrFail(params.idpj)
        try {
            const teps = await TestExecutionPlan.all()
            const showupdate = await Database.table('test_execution_plans')
                                        .innerJoin('projects','test_execution_plans.project_id','projects.id')
                                        .innerJoin('users_has_projects', 'test_execution_plans.project_id', 'users_has_projects.project_id')
                                        .select('test_execution_plans.id','test_execution_plans.tep_name', 'test_execution_plans.description', 
                                        'test_execution_plans.status', 'test_execution_plans.updated_at')
                                        .where('test_execution_plans.deleted', 'A')
                                        .whereNot('projects.deleted', 'D')
                                        .whereNot('users_has_projects.deleted', 'D')
                                        .where('test_execution_plans.project_id',project.id)
                                        .orderBy('test_execution_plans.updated_at', 'desc')
                                        .groupBy('test_execution_plans.id')
            
            return response.json({ "teps": showupdate})
        } catch (e) { 
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
    }

    async tep({ auth, response, params}) {
        const project = await Project.findOrFail(params.idpj)
        // try {
            const teps = await TestExecutionPlan.all()
            const showupdate2 = await Database.table('test_execution_plans')
                                        .innerJoin('projects','test_execution_plans.project_id','projects.id')
                                        .innerJoin('users_has_projects', 'test_execution_plans.project_id', 'users_has_projects.project_id')
                                        // .innerJoin('users','users_has_projects.user_id','users.id')
                                        .select('test_execution_plans.id','test_execution_plans.tep_name', 'test_execution_plans.description', 
                                        'test_execution_plans.status', 'test_execution_plans.updated_at')
                                        .where('test_execution_plans.deleted', 'A')
                                        .whereNot('projects.deleted', 'D')
                                        .whereNot('users_has_projects.deleted', 'D')
                                        // .whereNot('users.status_user', 'D')
                                        .where('users_has_projects.user_id', auth.user.id) 
                                        // .orWhere('users.role','Admin')
                                        .where('test_execution_plans.project_id',project.id)
                                        .orderBy('test_execution_plans.updated_at', 'desc')
                                        .groupBy('test_execution_plans.id')
            
            return response.json({ "teps": showupdate2})
        // } catch (e) { 
        //     return response.status(404).send({message: { error:'Resource not found.'} })
        // }
    }

    async createTEP({request,auth,response,params}) {
        const project = await Project.find(params.idpj)

        try {
                const teps = []
                teps.tep_name = request.input("tep_name")
                teps.description = request.input("description")
                teps.priority = request.input("priority")
                teps.test_type = request.input("test_type")
                teps.test_by = request.input("test_by")
                teps.status = request.input("status")
                teps.project_id = project.id

                // const userIds = [...(request.input("user") || [])]
                // const users = await User.getUsers(userIds)

                let tep = new TestExecutionPlan()
                const user = await tep.saveUser({project,teps,auth})
                return response.json({"test_execution_plans": tep, "user": user})
        } catch (e) {
            return response.status(400).send({message: { error:'Your create Test Execution Plans fail!'} })
        }
            
    }

    async createTCdirect({request,auth,response,params}) {
        const project = await Project.find(params.idpj)
        const teps = await TestExecutionPlan.find(params.id)
        const features = await FeatureHasTestExcution.findOrFail(params.idft)

        try {
            if (!project&&!teps&&!features) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project&&teps&&features) {
                if (features.deleted == 'A') {
                    // const 

                    let test_cases = new ExecutionsHasTesCase()
                    test_cases.tep_name = request.input("tep_name")
                    test_cases.description = request.input("description")
                    test_cases.priority = request.input("priority")
                    test_cases.test_type = request.input("test_type")
                    test_cases.test_by = request.input("test_by")
                    test_cases.status = request.input("status")
                    test_cases.project_id = project.id

                    // const userIds = [...(request.input("user") || [])]
                    // const users = await User.getUsers(userIds)

                    let tep = new TestExecutionPlan()
                    const user = await tep.saveUser({project,teps,auth})
                    return response.json({"test_execution_plans": tep, "user": user})
                }
                return response.json({message: 'Sorry, Test Execution Plans is already deleted.'});
            }
        } catch (e) {
            return response.status(400).send({message: { error:'Your create Test Execution Plans fail!'} })
        }
            
    }

    async updateTEP({request, params, auth, response}) {
          const { id } = params
          const teps = await TestExecutionPlan.find(id)
          const project = await Project.find(params.idpj)
          
          try {
              if (teps) {
                  teps.created_by = await auth.user.id
                  teps.modified_by = await auth.user.id
                  teps.project_id = project.id
                  teps.tep_name = request.input("tep_name")
                  teps.description = request.input("description")
                  teps.priority = request.input("priority")
                  teps.test_type = request.input("test_type")
                  teps.test_by = request.input("test_by")
                  teps.status = request.input("status")

                  await teps.save()
                //   return response.json({ "TEP": teps })
                  return response.json({message: 'Your update Test Execution Plan finish.' })
              }
              return response.status(404).send({message: { error:'Your update Test Execution Plan fail!'} })
          }
          catch (e) {
              console.log(e);
              return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }

    async viewTEP ({ params, response ,auth}) {
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        
        try { 
            if (!project&&!tep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project&&tep) {
                    if (tep.deleted == 'A') {
          // ถ้ามีการอนุญาติ user แล้ว ต้องแก้สิทธิ์เข้าถึง Project
            const showTEP = await Database.table('test_execution_plans')
                                .innerJoin('projects','test_execution_plans.project_id','projects.id')
                                .select('test_execution_plans.id','test_execution_plans.tep_name', 'test_execution_plans.description', 
                                'test_execution_plans.status', 'test_execution_plans.updated_at')
                                .where('test_execution_plans.deleted', 'A')
                                .whereNot('projects.deleted', 'D')
                                .where('test_execution_plans.project_id',project.id)
                                .where('test_execution_plans.id', tep.id)
                                .orderBy('test_execution_plans.updated_at', 'desc')

                        return response.json({"Project": project,"test_execution_plans": showTEP});
                    }
                    return response.json({message: 'Sorry, Test Execution Plans is already deleted.'});
                }
            return response.status(404).send({message: { error:'Resource not found.'} })
        } catch (error) {
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
    }

    async changestatusTEP ({ params, request , response }) {
        const { id } = params
        const tep = await TestExecutionPlan.find(id)
        const project = await Project.find(params.idpj)

        var status = request.input("status")
       
        try { 
            if (!tep&&!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (tep&&project) {
            
              if(tep.status == status){
                return response.json({message: "Current Test Execution Plan Status "+" "+ tep.tep_name +" : "+ tep.status })
              }
  
              tep.status = status
              await tep.save()
              return response.json({message: "Your Change Test Execution Plan Status" +" "+ tep.tep_name +" : "+ tep.status })
            }
            return response.status(404).send({message: { error:'Your Change status Test Execution Plan fail, Please your ckeck status Test Execution Plan.'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async deleteTEP({ params, response }) {
        const { id } = params
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(id)

        // const featuresTEP = await tep
        //                     .featuresTEPs()
        //                     .fetch()
        // const testcasesTEP = await tep
        //                     .testCasesTEPs()
        //                     .fetch()

        try { 
            if (!project && !tep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (project && tep) {
            if ( tep.deleted == 'D' ) {
                return response.json({ message: "Current Deleted Status" +" "+ tep.tep_name +" "+ tep.deleted +":Deleted"})
            }
        //Test Case ที่มี status = Unattempted เท่านั้น
                const testresultTEP = await Database
                                    .count('* as countTR')
                                    .from('executions_has_tes_cases')
                                    .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id') 
                                    .innerJoin('test_execution_plans', 'executions_has_tes_cases.tep_id', 'test_execution_plans.id')
                                    .select('test_execution_plans.id','executions_has_tes_cases.id','test_results.status')
                                    .where('test_results.deleted', 'Active')
                                    .whereNot({'test_results.status': 'Unattempted'})
                                    .whereNot('test_execution_plans.deleted', 'D')
                                    .where('executions_has_tes_cases.deleted', 'A')
                                    .where('test_execution_plans.id', tep.id)

                const count1 = testresultTEP[0].countTR
       
                if (count1 == 0) {

                    const checkstatus = await Database.from('test_execution_plans')
                                        .innerJoin('projects','test_execution_plans.project_id','projects.id')
                                        .select('projects.id','test_execution_plans.id')
                                        .where('test_execution_plans.deleted', 'A')
                                        .whereNot('projects.deleted', 'D')
                                        .where('test_execution_plans.id', tep.id)
                                        .where('projects.id', project.id)

                    const status = await TestExecutionPlan.findBy(checkstatus[0])

                        status.deleted = 'D'
                        await status.save()
                    return response.json({ message: "Your Deleted Status" +" "+ status.tep_name +" "+ status.deleted +":Deleted"})
                } 
            
                return response.json({ message: 'You can not delete Test Execution Plan because there are other test results.' });
            }
            return response.status(404).send({message: { error:'Resource not found.'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async undeletedTEP ({ params, response }) {
        const { id } = params
        const tep = await TestExecutionPlan.find(id)
        const project = await Project.find(params.idpj)
        try { 

            if (!tep&&!project) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (tep&&project) {
                if ( tep.deleted == 'A' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ tep.tep_name +" "+ tep.deleted +":Active"})
                }

                tep.deleted = 'A'
                await tep.save()
                return response.json({message: "Your Change Deleted Status" +" "+ tep.tep_name +" "+ tep.deleted +":Active"})
            }
            return response.status(404).send({message: { error:'Your Change Deleted status Test Execution Plan fail.'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async listfeature ({request, auth, response}) {
        const feature = await Feature.all()
        try {
            const feature = await Database.table('features')
                                            .innerJoin('projects', 'features.project_id', 'projects.id')
                                            .innerJoin('users_has_projects', 'features.project_id', 'users_has_projects.project_id')
                                            .innerJoin('users','users_has_projects.user_id','users.id')
                                            .select('features.project_id','features.id','features.feature_name','features.description',
                                            'features.modified_by','users.username','features.created_at','features.updated_at','features.status')
                                            .where('features.status', 'Active')
                                            .where('features.deleted' , 'A')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('users_has_projects.deleted' , 'D')
                                            .whereNot('users.status_user', 'D')
                                            .where('users_has_projects.user_id', auth.user.id)
                                            .orWhere('users.role', 'Admin')
                                            .groupBy('features.id')
                
                    return response.json({ "feature": feature})
            } catch (e) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }


    async addFeature({request,auth,response,params}) {
        const project = await Project.find(params.idpj)
        const teps = await TestExecutionPlan.find(params.id)

        try {
        //     if(project) {
            // const idfeature = [...(request.input("feature_id"))]
            // const features = await Feature.getFeature(idfeature)
        
            //ใน project นี้มี feature อะไรบ้าง&
            // const checkFT = await Feature
            // Database.from('features')
                                    // .with('projects', 'features.project_id', 'projects.id')
                                    // .select('features.id')
                                    // .where('features.deleted', 'A')
                                    // .whereNot('projects.deleted', 'D')
                                    // .where('features.project_id', project.id)
                                    // console.log(checkFT);
                                    // return
                                    

                    // for(let i in features.rows) {
                    //     const feature = features.rows[i]
                    //     console.log(feature.id);
                    //     return
                    // }
                    
                    // if (idfeature.features == 'request.input("feature_id")') {
                    // } else if ( editTep.test_result_id != 'request.input("test_result_id")') {

                    // }

                    // let tep = await TestExecutionPlan.findBy('id', teps.id);

                    // if(!tep){
                    //     return response.status(404).send({message: { error:'Resource not found.'} })
                    // }

                    // const feature = await tep.saveFeature({teps,auth,features,project})
                    // return response.json({ message: 'Add Feature Finish!' })
                // }
                // return response.status(404).send({message: { error:'Resource not found.'} })
                                    
            // }

            const idfeature = [...(request.input("feature_id"))]
            const features = await Feature.getFeature(idfeature)

            // let tep = await TestExecutionPlan.findBy('id', teps.id);

                if(!project&&!teps){
                    return response.status(404).send({message: { error:'Resource not found.'} })
                }

            const feature = await teps.saveFeature({teps,auth,features,project})
            return response.json({ message: 'Add Feature Finish!' })
        }
        catch (e) {
            return response.status(400).send({message: { error:'Your create Feature fail.'} })
        }
    }

    async featuresTEPs({ auth, response, params}) {
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        try {
            if (!project&&!tep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
        } else if (tep.deleted == 'A') {

            const checkHistory = await Database.count('TephTc_id as countH')
                                            .from('histories')
                                            .innerJoin('executions_has_tes_cases','histories.TephTc_id','executions_has_tes_cases.id')
                                            .innerJoin('test_execution_plans','executions_has_tes_cases.tep_id','test_execution_plans.id')
                                            .select('histories.TephTc_id')
                                            .where('executions_has_tes_cases.deleted' , 'A')
                                            .whereNot('test_execution_plans.deleted' , 'D')
                                            .where('executions_has_tes_cases.tep_id', tep.id)
                                            .where('executions_has_tes_cases.project_id', project.id)

                const checkH = checkHistory[0].countH

            if(checkH == 0) {
                //ยังไม่มีการเปลี่ยนสถานะ Test Result ของ Test Case
                const feature = await Database.table('feature_has_test_excutions')
                                        .innerJoin('test_execution_plans','feature_has_test_excutions.tep_id','test_execution_plans.id')
                                        .innerJoin('projects', 'feature_has_test_excutions.project_id', 'projects.id')
                                        .innerJoin('features', 'feature_has_test_excutions.feature_id', 'features.id')
                                        .select('feature_has_test_excutions.project_id','feature_has_test_excutions.tep_id','feature_has_test_excutions.feature_id','features.feature_name', 'feature_has_test_excutions.updated_at','feature_has_test_excutions.deleted')
                                        .where('feature_has_test_excutions.deleted' , 'A')
                                        .whereNot('test_execution_plans.deleted' , 'D')
                                        .whereNot('projects.deleted' , 'D')
                                        .whereNot('features.deleted' , 'D')
                                        .where('feature_has_test_excutions.tep_id', tep.id)
                                        .where('feature_has_test_excutions.project_id', project.id)
                                        .groupBy('feature_has_test_excutions.feature_id')
                                        .orderBy('feature_has_test_excutions.updated_at', 'desc')

                const countAmount1 = await Database.count('test_cases_id')
                                        .from('executions_has_tes_cases')
                                        .select('executions_has_tes_cases.feature_id')
                                        .where('executions_has_tes_cases.deleted' , 'A')
                                        .where('executions_has_tes_cases.tep_id', tep.id)
                                        .where('executions_has_tes_cases.project_id', project.id)
                                        .groupBy('executions_has_tes_cases.feature_id')

                return response.json({ "test_execution_plans": tep, "feature": feature, "amount": countAmount1})
            } else if(checkH != 0) {

                //มีการเปลี่ยนสถานะ Test Result ของ Test Case แล้ว
                const history = await Database.table('feature_has_test_excutions')
                                                .innerJoin('executions_has_tes_cases','feature_has_test_excutions.tep_id','executions_has_tes_cases.tep_id')
                                                .innerJoin('histories', 'histories.TephTc_id', 'executions_has_tes_cases.id')
                                                .innerJoin('test_execution_plans','executions_has_tes_cases.tep_id','test_execution_plans.id')
                                                .innerJoin('projects', 'feature_has_test_excutions.project_id', 'projects.id')
                                                .innerJoin('features', 'feature_has_test_excutions.feature_id', 'features.id')
                                                .select('executions_has_tes_cases.project_id','executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','features.feature_name','histories.updated_at')
                                                .where('executions_has_tes_cases.deleted' , 'A')
                                                .whereNot('feature_has_test_excutions.deleted', 'D')
                                                .whereNot('test_execution_plans.deleted' , 'D')
                                                .whereNot('projects.deleted' , 'D')
                                                .whereNot('features.deleted' , 'D')
                                                .where('executions_has_tes_cases.tep_id', tep.id)
                                                .where('feature_has_test_excutions.project_id', project.id)
                                                .groupBy('executions_has_tes_cases.feature_id')
                                                .orderBy('histories.updated_at', 'desc')

                const countAmount2 = await Database.count('test_cases_id')
                                                .from('executions_has_tes_cases')
                                                .select('executions_has_tes_cases.feature_id')
                                                .where('executions_has_tes_cases.deleted' , 'A')
                                                .where('executions_has_tes_cases.tep_id', tep.id)
                                                .where('executions_has_tes_cases.project_id', project.id)
                                                .groupBy('executions_has_tes_cases.feature_id')

                return response.json({ "test_execution_plans": tep, "feature": history, "amount": countAmount2})
                }
            }
            return response.json({ message: 'FeaturesTEP has been deleted.'})
        } catch (e) { 
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
    }

    async featureTEPs({ response, params}){
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        try {
            if (!project&&!tep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
        } else if (tep.deleted == 'A') {
                // const features = await tep
                //                 .featuresTEPs()
                //                 .fetch()

                const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)
                
                const showdeleted = await Database
                                    .table('feature_has_test_excutions')
                                    .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                    .innerJoin('projects', 'feature_has_test_excutions.project_id', 'projects.id')
                                    .select('feature_has_test_excutions.project_id','feature_has_test_excutions.tep_id','feature_has_test_excutions.feature_id')
                                    .where('feature_has_test_excutions.deleted' , 'A')
                                    .whereNot('test_execution_plans.deleted' , 'D')
                                    .where('feature_has_test_excutions.tep_id', tep.id)
                                    .where('feature_has_test_excutions.project_id', project.id)
                                    .where('feature_has_test_excutions.feature_id', idfeature.id)
                                    .groupBy('feature_has_test_excutions.feature_id')

                return response.json({ "test_execution_plans": tep, "featuresTEPs": showdeleted})
            }
            return response.json({ message: 'FeaturesTEP has been deleted.'})
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async deletefeatureTEP({ params, response, auth }) {
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id)
        // const features = await tep
        //                     .featuresTEPs()
        //                     .fetch()
        // const testcases = await tep
        //                     .testCasesTEPs()
        //                     .fetch()
        
        const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)

        try { 
            if (!project&&!tep&&!idfeature) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (idfeature) {
            if (tep.deleted == 'D' && idfeature.deleted == 'D') {
                return response.json({message: "Current Deleted Status" +" "+ idfeature.feature_id +" "+ idfeature.deleted +":Deleted"})
            }
                        //Test Case ที่มี status = Unattempted เท่านั้น
                        const featureTEP = await Database
                                            .count('* as countFT')
                                            .from('executions_has_tes_cases')
                                            .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                            .innerJoin('projects', 'executions_has_tes_cases.project_id', 'projects.id') 
                                            .select('executions_has_tes_cases.feature_id','executions_has_tes_cases.test_result_id')
                                            .where('executions_has_tes_cases.deleted', 'A')
                                            .where('test_results.deleted', 'Active')
                                            .whereNot({'test_results.status': 'Unattempted'})
                                            .where('executions_has_tes_cases.tep_id', tep.id)
                                            .where('executions_has_tes_cases.feature_id' , idfeature.id)
                                            .where('executions_has_tes_cases.project_id', project.id)
                        
                        const count1 = featureTEP[0].countFT
                            if (count1 == 0) {
                                const idtephasft = await Database
                                                    .from('feature_has_test_excutions')
                                                    .where('feature_has_test_excutions.deleted', 'A')
                                                    .where('feature_has_test_excutions.tep_id', tep.id)
                                                    .where('feature_has_test_excutions.feature_id', idfeature.id)
                                                    .where('feature_has_test_excutions.project_id', project.id)

                                const featureId = await FeatureHasTestExcution.findBy(idtephasft[0])
                                featureId.deleted = 'D'
                                await featureId.save()
                                return response.json({ message: "Your Deleted Status" +" "+ featureId.id +" "+ featureId.deleted +":Deleted"})
                            } 
                return response.json({ message: 'You can not delete Feature has Test Execution Plan because there are other test results.' });
            }
            return response.status(404).send({message: { error:'Your deleted Feature has Test Execution Plan fail!'} })
        } catch (error) {
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
    }

    async undeletedfeatureTEP ({ params, response }) {
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id)
        const features = await tep
                            .featuresTEPs()
                            .fetch()
        const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)

        try { 
            if (!project&&!tep&&!idfeature) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (idfeature) {
                    if ( idfeature.deleted == 'A' ) {
                        return response.json({ message: "Current Deleted Status ID:" + idfeature.id +" "+ idfeature.deleted +":Active"})
                    }

                    idfeature.deleted = 'A'
                    await idfeature.save()
                    return response.json({message: "Your Change Deleted Status ID:" + idfeature.id +" "+ idfeature.deleted +":Active"})
                }
            return response.status(404).send({message: { error:'Your Change Deleted status Feature has Test Execution Plan fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async addTestCase({request,auth,response,params}) {
        const projects = await Project.find(params.idpj)
        const teps = await TestExecutionPlan.findOrFail(params.id);
        const features = await FeatureHasTestExcution.findOrFail(params.idft);


        var idtestcases = request.input("testcases_id") 

        if(typeof idtestcases == 'string') {
            var idtestcases2 = [idtestcases];
            idtestcases = idtestcases2;
        }
        // console.log(idtestcases);
        
        const testcases = await TestCase.getTestcase(idtestcases)

        const testresults = []
        testresults.id = request.input("testresult_id")
        testresults.id = '1'
        
        const testresult = await TestResult.findBy('id', testresults.id) 

            if(!projects){
                return response.status(404).send({message: { error:'Resource not found project.'} })
            }if(!teps){
                return response.status(404).send({message: { error:'Resource not found tep.'} })
            }if(!features){
                return response.status(404).send({message: { error:'Resource not found feature.'} })
            }if(!testresults){
                return response.status(404).send({message: { error:'Resource not found testresult.'} })
            }if(!testcases) {
                return response.status(404).send({message: { error:'Resource not found test case.'} })
            }
            
        const testcase = await teps.saveTestCase({teps,auth,features,idtestcases,testresults,projects})
        return response.json({ "test_execution_plans": teps, "featuresTEPs": features, message: "Test Case add to Test Execution Plans finish.", "TestResult": testresult })

    }

    async updateTestCase({request,auth,response,params}) {
        const projects = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)
        const editTep = await ExecutionsHasTesCase.findOrFail(params.idtc)

        try {
            if(!projects&&!tep&& !idfeature && !editTep){
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (editTep.deleted == 'A') {
                editTep.created_by = await auth.user.id
                editTep.modified_by = await auth.user.id
                editTep.test_type = request.input("test_type")
                editTep.status = request.input("status")
                editTep.test_result_id = request.input("test_result_id")
            
                try { 
                    if(!projects){
                        return response.status(404).send({message: { error:'Resource not found project.'} })
                    }if(!tep){
                        return response.status(404).send({message: { error:'Resource not found tep.'} })
                    }if(!idfeature){
                        return response.status(404).send({message: { error:'Resource not found idfeature.'} })
                    }if(!editTep){
                        return response.status(404).send({message: { error:'Resource not found editTep.'} })
                    } 
                    else 
                    if (editTep.test_result_id == 'request.input("test_result_id")') {
                        } else if ( editTep.test_result_id != 'request.input("test_result_id")') {

                            const checkHistory = await Database
                                                .count('* as countH')
                                                .from('histories')
                                                .innerJoin('executions_has_tes_cases', 'histories.TephTc_id', 'executions_has_tes_cases.id') 
                                                .where('executions_has_tes_cases.deleted', 'A')
                                                .where('histories.TephTc_id', editTep.id)
                                                .where('histories.test_result_id' , editTep.test_result_id)

                            const checkH = checkHistory[0].countH

                        if (checkH == 0) {
                                let historyTc = new History()
                                historyTc.created_by = await auth.user.id
                                historyTc.modified_by = await auth.user.id
                                historyTc.TephTc_id = editTep.id
                                historyTc.test_result_id = editTep.test_result_id
                                const HT = await historyTc.save()
                                // return response.json({ message: "Create History Finish"})
                            }
                        }
                    } catch (error) {
                        return response.status(400).send({message: { error:'Bad Request!'} })
                    }

                const updateTC = await editTep.save()
                return response.json({message: 'Your update Test Case finish.' })
                return response.json({ "test_execution_plans": tep,"featuresTEPs": idfeature, "TestCase": updateTC})
            }
            return response.status(400).send({message: { error:'Your update Test Case fail!'} })
        } catch (e) {
            console.log(e);
            return response.status(404).send({message: { error:'Resource not found.'} })
        }

    }

    async testcasesTEPs({ auth, response, params}) {
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        const feature = await FeatureHasTestExcution.findOrFail(params.idft);
        try {
            if (!project&&!tep&&!feature) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (tep.deleted == 'A') {
            
                const showtc = await Database.table('executions_has_tes_cases')
                                    .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                    .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                    .innerJoin('projects', 'executions_has_tes_cases.project_id', 'projects.id')
                                    .innerJoin('test_cases', 'executions_has_tes_cases.test_cases_id', 'test_cases.id')
                                    .innerJoin('test_results','executions_has_tes_cases.test_result_id','test_results.id')
                                    .innerJoin('users', 'executions_has_tes_cases.modified_by','users.id')
                                    .select('executions_has_tes_cases.feature_id','executions_has_tes_cases.test_cases_id','test_cases.testcase_No',
                                    'test_cases.testCase_name','executions_has_tes_cases.test_type','executions_has_tes_cases.status',
                                    'executions_has_tes_cases.modified_by','users.username','executions_has_tes_cases.created_at','executions_has_tes_cases.updated_at',
                                    'executions_has_tes_cases.test_result_id','test_results.name')
                                    .where('executions_has_tes_cases.deleted', 'A')
                                    .whereNot('feature_has_test_excutions.deleted' , 'D')
                                    .whereNot('test_execution_plans.deleted' , 'D')
                                    .whereNot('projects.deleted' , 'D')
                                    .whereNot('test_cases.deleted' , 'D')
                                    .whereNot('test_results.deleted' , 'Delete')
                                    .where('executions_has_tes_cases.tep_id', tep.id)
                                    .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id')
                                    .where('executions_has_tes_cases.project_id', project.id)
                                    .where('executions_has_tes_cases.feature_id', feature.id)
                                    
                return response.json({ "test_execution_plans": tep, "TestCase": showtc})
                }
            
                return response.status(404).send({message: { error:'Resource not found.'} })
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async testcaseTEPs({ response, params}){
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)
        const idtestcase = await ExecutionsHasTesCase.findOrFail(params.idtctep)
        try {
            if (!tep && !idfeature && !idtestcase) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (idtestcase.deleted == 'A') {

            const showtcId = await Database.table('executions_has_tes_cases')
                            .innerJoin('feature_has_test_excutions','executions_has_tes_cases.tep_id'&&'executions_has_tes_cases.feature_id','feature_has_test_excutions.tep_id'&&'feature_has_test_excutions.feature_id')
                            .innerJoin('test_execution_plans','executions_has_tes_cases.tep_id','test_execution_plans.id')
                            .innerJoin('projects', 'executions_has_tes_cases.project_id', 'projects.id')
                            .innerJoin('test_cases', 'executions_has_tes_cases.test_cases_id', 'test_cases.id')
                            .innerJoin('test_results','executions_has_tes_cases.test_result_id','test_results.id')
                            .select('executions_has_tes_cases.feature_id','executions_has_tes_cases.test_cases_id','test_cases.testcase_No',
                            'test_cases.testCase_name','executions_has_tes_cases.test_type','executions_has_tes_cases.status',
                            'executions_has_tes_cases.modified_by','executions_has_tes_cases.created_at','executions_has_tes_cases.updated_at',
                            'executions_has_tes_cases.test_result_id','test_results.name')
                            .where('executions_has_tes_cases.deleted', 'A')
                            .whereNot('feature_has_test_excutions.deleted', 'D')
                            .whereNot('test_execution_plans.deleted', 'D')
                            .whereNot('projects.deleted' , 'D')
                            .whereNot('test_cases.deleted' , 'D')
                            .whereNot('test_results.deleted' , 'Delete')
                            .where('executions_has_tes_cases.tep_id', tep.id)
                            .where('executions_has_tes_cases.feature_id', idfeature.id)
                            .where('executions_has_tes_cases.test_cases_id', idtestcase.id)
                            .where('executions_has_tes_cases.project_id', project.id)
                            .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id')
                            
            return response.json({ "test_execution_plans": tep, "TestCase_Id": showtcId})
            }
            return response.json({ "test_execution_plans": tep, message: 'Test Case has been deleted.'})
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async deletedTCTEP({ params, response }) {
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)
        const idTCTEP = await ExecutionsHasTesCase.findOrFail(params.idtctep)
        // const features = await tep
        //                 .featuresTEPs()
        //                 .fetch()
        // const testcases = await tep
        //                 .testCasesTEPs()
        //                 .fetch()
                
        
        try { 
            if (!project&&!tep&&!idfeature&&!idTCTEP) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (idTCTEP.test_result_id == '1') {
                if ( idTCTEP.deleted == 'D' ) {
                    return response.json({ message: "Current Deleted Status ID:" + idTCTEP.id +" "+ idTCTEP.deleted +":Deleted"})
                } 

                idTCTEP.deleted = 'D'
                await idTCTEP.save()
                return response.json({message: "Your Change Deleted Status ID:" + idTCTEP.id +" "+ idTCTEP.deleted +":Deleted"})
                }
            return response.status(404).send({message: { error:'Cannot delete because it is not an unattempted name!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async undeletedTCTEP ({ params, response }) {
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)
        const idTCTEP = await ExecutionsHasTesCase.findOrFail(params.idtctep)
        // const features = await tep
        //                 .featuresTEPs()
        //                 .fetch()
        // const testcases = await tep
        //                 .testCasesTEPs()
        //                 .fetch()

            try { 
                if (!project&&!tep&&!idfeature&&!idTCTEP) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (idTCTEP) {
                if ( idTCTEP.deleted == 'A' ) {
                    return response.json({ message: "Current Deleted Status ID:" + idTCTEP.id +" "+ idTCTEP.deleted +":Active"})
                }

                idTCTEP.deleted = 'A'
                await idTCTEP.save()
                return response.json({message: "Your Change Deleted Status ID:" + idTCTEP.id +" "+ idTCTEP.deleted +":Active"})
                }
            return response.status(404).send({message: { error:'Your Change Deleted name Feature has Test Execution Plan fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async viewHistory({ response, params}){
        const project = await Project.find(params.idpj)
        const tep = await TestExecutionPlan.findOrFail(params.id);
        const idfeature = await FeatureHasTestExcution.findOrFail(params.idft)
        const tcTep = await ExecutionsHasTesCase.findOrFail(params.idtc)

        try {
            if (!project&&!tep&&!idfeature&&!tcTep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (tcTep.deleted == 'A') {
                const features = await tep
                                .featuresTEPs()
                                .fetch()
                const testcases = await tep
                                .testCasesTEPs()
                                .fetch()

                const showHistory = await Database.table('histories')
                                .innerJoin('executions_has_tes_cases','histories.TephTc_id','executions_has_tes_cases.id')
                                .innerJoin('projects', 'executions_has_tes_cases.project_id', 'projects.id')
                                .select('histories.updated_at','histories.test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .where('histories.TephTc_id', tcTep.id)
                                .where('executions_has_tes_cases.project_id', project.id)
                
                const idtestcase = await Database.table('histories')
                                .innerJoin('executions_has_tes_cases','histories.TephTc_id','executions_has_tes_cases.id')
                                .innerJoin('projects', 'executions_has_tes_cases.project_id', 'projects.id')
                                .select('executions_has_tes_cases.id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .where('histories.TephTc_id', tcTep.id)
                                .groupBy('executions_has_tes_cases.id')
                                .where('executions_has_tes_cases.project_id', project.id)
                                
                return response.json({ "test_execution_plans": tep, "Feature": idfeature, "TestCase": idtestcase, "History": showHistory})
            }
            return response.json({ "test_execution_plans": tep, message: 'Test Case has Test Execution Plan has been deleted.'})
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

}

module.exports = TepController
