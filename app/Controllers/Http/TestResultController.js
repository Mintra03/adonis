'use strict'
const TestResult = use('App/Models/TestResult');

class TestResultController {

    async testResults({ auth, response}) {
      try {
          const testResults = await TestResult.all()
          // console.log(projects);
          
          return response.json({ "test_results":testResults })
        }
        catch (e) {
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
    }

    async create({request,auth,response}) {
        try {
            const name = request.input("name")
            
            let testResult = new TestResult()
            testResult.name = name
            testResult.deleted = 'Active'

            testResult = await testResult.save()
            // return response.json({message: 'Your create Test Result finish.'})
            return response.json({message: 'Your create Test Result finish.'})
        }
        catch (e) {
            return response.status(400).send({message: { error:'Your create Test Result fail.'} })
        }
    }

    async update({request, params, auth, response}) {
          const { id } = params
          const testresult = await TestResult.find(id)
          
          try {
              if (testresult) {
                  testresult.created_by = await auth.user.id
                  testresult.modified_by = await auth.user.id
                  testresult.name = request.input("name")

                  await testresult.save()
                  return response.json({message: 'Your update Test Result finish.' })
              }
              return response.status(404).send({message: { error:'Resource not found.'} })
          }
          catch (e) {
              console.log(e);
              return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }

    async viewtestresult ({ params, response ,auth}) {
      const testresult = await TestResult.findOrFail(params.id);
      try {
          if(testresult) {
              // ถ้ามีการอนุญาติ user แล้ว ต้องแก้สิทธิ์เข้าถึง TestCase
                return response.json({ testresult: testresult });
          }
            return response.status(404).send({message: { error:'Resource not found.'} })
      } catch (error) {
        return response.status(404).send({message: { error:'Resource not found.'} })
      }
    }

    async delete({ params, response }) {
          const { id } = params
          const testresult = await TestResult.find(id)
          
          try { 
              if (!testresult) {
                return response.status(404).send({message: { error:'Resource not found.'} })
              } else if (testresult) {
              if ( testresult.deleted == 'Delete' ) {
                  return response.json({ message: "Current Deleted Status" +" "+ testresult.name +" "+ testresult.deleted +":Deleted"})
              }

              testresult.deleted = 'Delete'
              await testresult.save()
              return response.json({message: "Your Deleted Status" +" "+ testresult.name +" "+ testresult.deleted +":Deleted"})
              }
            return response.status(404).send({message: { error:'Your deleted Test Result fail!'} })
      } catch (error) {
        return response.status(400).send({message: { error:'Bad Request!'} })
      }
    }

    async undelete ({ params, response }) {
      const { id } = params
      const testresult = await TestResult.find(id)
      try { 

          if (!testresult) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          } else if (testresult) {
          if ( testresult.deleted == 'Active' ) {
              return response.json({ message: "Current Deleted Status" +" "+ testresult.name +" "+ testresult.deleted +":Active"})
          }

          testresult.deleted = 'Active'
          await testresult.save()
          return response.json({message: "Your Change Deleted Status" +" "+ testresult.name +" "+ testresult.deleted +":Active"})
          }
          return response.status(404).send({message: { error:'Your Change Deleted name Test Result fail!'} })
      } catch (error) {
        return response.status(400).send({message: { error:'Bad Request!'} })
      }
    }

}

module.exports = TestResultController
