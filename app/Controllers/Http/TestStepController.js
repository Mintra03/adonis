'use strict'
const TestStep = use('App/Models/TestStep')

class TestStepController {

  async teststeps ({request, auth, response}) {
    try {
        const teststeps = await TestStep.all()
        
        return response.json({ "test_steps":teststeps })
      }
      catch (e) {
        return response.status(404).send({message: { error:'Resource not found.'} })
      }
    }

    async create({request, auth, response}) {
        try {
                const project_id = request.input("project_id")
                const feature_id = request.input("feature_id")
                const testcase_id = request.input("testcase_id")
                const teststep_name = request.input("testCase_name")
                const description = request.input("description")
                const status = request.input("status")
                const deleted = request.input("deleted")
                
                let teststep = new TestStep()
                teststep.created_by = await auth.user.id
                teststep.modified_by = await auth.user.id
                teststep.project_id = project_id
                teststep.feature_id = feature_id
                teststep.testcase_id = testcase_id
                teststep.teststep_name = teststep_name
                teststep.description = description
                teststep.status = status
                teststep.deleted = 'A'
                
                teststep = await teststep.save()
                return response.json({message: 'Your create TestStep finish.' })
        }
        catch (e) {
            return response.status(400).send({message: { error:'Your create TestStep fail.'} })
        }
    }

    async update({request, params, auth, response}) {
        console.log(params);
            const { id } = params
            const teststep = await TestStep.find(id)
            
            try {
                if (teststep) {
                    teststep.created_by = await auth.user.id
                    teststep.modified_by = await auth.user.id
                    teststep.teststep_name = request.input("teststep_name")
                    teststep.description = request.input("description")
                    teststep.status = request.input("status")
                    teststep.deleted = request.input("deleted")

                    await teststep.save()
                    return response.json({message: 'Your update TestStep finish.' })
                }
                return response.status(400).send({message: { error:'Your update TestStep fail.'} })
            }
            catch (e) {
                console.log(e);
                return response.status(404).send({message: { error:'Resource not found.'} })
            }
    }

    async viewteststep ({ params, response ,auth}) {
        const teststep = await TestStep.findOrFail(params.id);
        try {
            if(teststep) {
                // ถ้ามีการอนุญาติ user แล้ว ต้องแก้สิทธิ์เข้าถึง TestCase
                return response.json({ teststep: teststep });
            }
            return response.status(404).send({message: { error:'Resource not found.'} })
        } catch (error) {
            return response.status(403).send({message: { error:'Sorry, you do not have permission to access.'}})
        }
    }

    async changestatus ({ params, request , response }) {
    const { id } = params
    const teststeps = await TestStep.find(id)
    var status = request.input("status")
    
    try { 
            if (!teststeps) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (teststeps) {
                    if(teststeps.status == status){
                    return response.json({message: "Current teststeps status of the user"})
                    }

                    teststeps.status = status
                    await teststeps.save()
                    return response.json({message: "Current TestCase Status" +" "+ teststeps.teststep_name +" : "+ teststeps.status })
                }
            return response.status(400).send({message: { error:'Your Change status TestStep fail, Please your ckeck status TestStep.'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async delete({ params, session, response }) {
            const { id } = params
            const teststep = await TestStep.find(id)
            try { 
                if (!teststep) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (teststep) {
                if ( teststep.deleted == 'D' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ teststep.teststep_name +" "+ teststep.deleted +":Deleted"})
                }

                teststep.deleted = 'D'
                await teststep.save()
                return response.json({message: "Your Deleted Status" +" "+ teststep.teststep_name +" "+ teststep.deleted +":Deleted"})
                }
            
            // await user.delete()
            // session.flash({ notification: 'User Deleted!' })
        
            return response.status(404).send({message: { error:'Your deleted TestStep fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async undelete ({ params, response }) {
        const { id } = params
        const teststep = await TestStep.find(id)
        try { 

            if (!teststep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (teststep) {
                if ( teststep.deleted == 'A' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ teststep.teststep_name +" "+ teststep.deleted +":Active"})
                }

                teststep.deleted = 'A'
                    await teststep.save()
                    return response.json({message: "Your Change Deleted Status" +" "+ teststep.teststep_name +" "+ teststep.deleted +":Active"})
                }
            return response.status(404).send({message: { error:'Your Change Deleted status TestStep fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

}

module.exports = TestStepController
