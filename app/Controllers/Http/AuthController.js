'use strict'

const User = use('App/Models/User');
const { validate } = use('Validator')

class AuthController {

    async login({request, auth, response}) {
        const email = request.input("email")
        const password = request.input("password");
        
        // try {
          if (await auth.attempt(email, password)) {
            let user = await User.findBy('email', email)
            let accessToken = await auth.generate(user)
            // return response.json({message: 'You LogIn Finish.'})
            return response.json({"user":user, "access_token": accessToken})
          }
        // } catch (e) {
        //   // return response.status(400).send({ message: { error: 'You entered the code incorrectly. Please enter the code again.' } })
        //   return response.status(404).send({message: { error:'Resource not found.'} })
        // }
    }

    async logout({ auth, response}) {
      const check = await auth.check();

      try {
          if (check) {
            const token = await auth.getAuthHeader();
            await auth.authenticator("jwt").revokeTokens([token]);
            return response.json({ message: 'You have been logged out.' });
        }
        return response.status(401).send({ message: { error: 'Unauthorized, You have not been logged out.' } })
      } catch (e) {
        return response.status(401).send({ message: { error: 'Unauthorized, You have not been logged out.' } })
      }
    }

    
}

module.exports = AuthController
