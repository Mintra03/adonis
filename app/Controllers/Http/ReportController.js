'use strict'

const Database = use('Database')
const Report = use('App/Models/Report')
const TestExecutionPlan = use('App/Models/TestExecutionPlan');
const Priority = use('App/Models/PrioritySetting')

class ReportController {

  async testprogress ({response, params}) {
      const tep = await TestExecutionPlan.findOrFail(params.id);

      try {
            if(!tep) {
                  return response.status(404).send({message: { error:'Resource not found.'} })
            } else if(tep.deleted == 'A') {
                  const countSum = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id')

                  const counstatus1 = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .where('executions_has_tes_cases.test_result_id', '1')
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id')
                        const avg1 = counstatus1/countSum*100 

                  const counstatus2 = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .where('executions_has_tes_cases.test_result_id', '2')
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id')
                        const avg2 = counstatus2/countSum*100
      
                  const counstatus3 = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .where('executions_has_tes_cases.test_result_id', '3')
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id') 
                        const avg3 = counstatus3/countSum*100          
      
                  const counstatus4 = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .where('executions_has_tes_cases.test_result_id', '4')
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id')
                        const avg4 = counstatus4/countSum*100

                  const counstatus5 = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .where('executions_has_tes_cases.test_result_id', '5')
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id')
                        const avg5 = counstatus5/countSum*100

                  const counstatus6 = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .where('executions_has_tes_cases.test_result_id', '6')
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id')
                        const avg6 = counstatus6/countSum*100

                  const counstatus7 = await Database.table('executions_has_tes_cases')
                                .innerJoin('feature_has_test_excutions', 'executions_has_tes_cases.tep_id' & 'executions_has_tes_cases.feature_id', 'feature_has_test_excutions.tep_id' & 'feature_has_test_excutions.feature_id')
                                .innerJoin('test_execution_plans', 'feature_has_test_excutions.tep_id', 'test_execution_plans.id')
                                .innerJoin('test_results', 'executions_has_tes_cases.test_result_id', 'test_results.id')
                                .select('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .where('executions_has_tes_cases.deleted', 'A')
                                .whereNot('feature_has_test_excutions.deleted' , 'D')
                                .whereNot('test_execution_plans.deleted' , 'D')
                                .whereNot('test_results.deleted', 'Delete')
                                .where('executions_has_tes_cases.tep_id', tep.id)
                                .where('executions_has_tes_cases.test_result_id', '7')
                                .groupBy('executions_has_tes_cases.tep_id','executions_has_tes_cases.feature_id','test_cases_id','test_result_id')
                                .getCount('test_result_id')
                        const avg7 = counstatus7/countSum*100

                        const percentage = avg1+avg2+avg3+avg4+avg5+avg6+avg7

                  return response.json({ message: 'Test Result', "Unattempted": avg1.toFixed(2), "Passed": avg2.toFixed(2), "Passed Retest": avg3.toFixed(2), 
                                    "Failed": avg4.toFixed(2), "Failed Retest": avg5.toFixed(2), "Block Dependency": avg6.toFixed(2),
                                    "Block No Code Drop": avg7.toFixed(2), "Total Test Result": countSum, "Percentage": percentage.toFixed(2)})
            }
            return response.json({ message: 'Test Execution Plans is deleted!'})
      } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
      
    }
  
    async OpenPriority({response, params, request, auth}) {
        const tep = await TestExecutionPlan.findOrFail(params.id);

        try {
            if (!tep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (tep.deleted == 'A') {

                  const priority = await Database.table('priority_settings')
                                          .innerJoin('test_execution_plans', 'priority_settings.tep_id', 'test_execution_plans.id')
                                          .select('priority_name')
                                          .where('priority_settings.deleted', 'A')
                                          .whereNot('test_execution_plans.deleted' , 'D')
                                          .where('priority_settings.tep_id', tep.id)

                  return response.json({ "Priority": priority})
                  }
                  return response.json({ message: 'Priority is deleted!'})
            } catch (e) { 
                return response.status(400).send({message: { error:'Bad Request!'} })
            }
    }

    async OpenSeverity({response, params}) {
        const tep = await TestExecutionPlan.findOrFail(params.id);

        try {
            if (!tep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (tep.deleted == 'A') {

                  const servity = await Database.table('servity_settings')
                                          .innerJoin('test_execution_plans', 'servity_settings.tep_id', 'test_execution_plans.id')
                                          .select('servity_name')
                                          .where('servity_settings.deleted', 'A')
                                          .whereNot('test_execution_plans.deleted' , 'D')
                                          .where('servity_settings.tep_id', tep.id)

                  return response.json({ "Servity": servity})
                  }
                  return response.json({ message: 'Servity is deleted!'})
            } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
            }
    }

    async OpendefectSeverity({response, params}) {
        const tep = await TestExecutionPlan.findOrFail(params.id);

        try {
            if (!tep) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (tep.deleted == 'A') {

                  const defectstatus = await Database.table('defect_status_settings')
                                          .innerJoin('test_execution_plans', 'defect_status_settings.tep_id', 'test_execution_plans.id')
                                          .select('defect_status_name')
                                          .where('defect_status_settings.deleted', 'A')
                                          .whereNot('test_execution_plans.deleted' , 'D')
                                          .where('defect_status_settings.tep_id', tep.id)

                  return response.json({ "Defectstatus": defectstatus})
                  }
                  return response.json({ message: 'Defectstatus is deleted!'})
            } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
            }
      }

}

module.exports = ReportController
