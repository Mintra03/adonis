'use strict'

const DefectStatus = use('App/Models/DefectStatusSetting');
const TEP = use('App/Models/TestExecutionPlan')
const Database = use('Database')

class DefectStatusSettingController {

      async defectStatus ({request, auth, response}) {
        const defect_status = await DefectStatus.all()
          try {
              const showdefectStatus = await Database.table('defect_status_settings')
                                              .innerJoin('test_execution_plans', 'defect_status_settings.tep_id', 'test_execution_plans.id')
                                              .select('defect_status_name')
                                              .where('defect_status_settings.deleted' , 'A')
                                              .whereNot('test_execution_plans.deleted' , 'D')
                  
                  return response.json({ "defect_status": defect_status})
            }
            catch (e) {
              return response.status(404).send({message: { error:'Resource not found.'} })
            }
      }

      async create({request,auth,response}) {
        try {
              const tep_id = request.input("tep_id")
              const defect_status_name = request.input("defect_status_name")

              let defect_status = new DefectStatus()
              defect_status.created_by = await auth.user.id
              defect_status.modified_by = await auth.user.id
              defect_status.tep_id = tep_id
              defect_status.defect_status_name = defect_status_name
              defect_status.deleted = 'A'
              
              defect_status = await defect_status.save()
              return response.json({message: 'Your create Defect Status finish.' })
          }
          catch (e) {
            return response.status(400).send({message: { error:'Your create Defect Status fail!'} })
          }
      }

      async update({request, params, auth, response}){
            const { id } = params
            const defect_status = await DefectStatus.find(id)
              try {
                if (defect_status) {
                    defect_status.created_by = await auth.user.id
                    defect_status.modified_by = await auth.user.id
                    defect_status.tep_id = request.input("tep_id")
                    defect_status.defect_status_name = request.input("defect_status_name")
                    
                    await defect_status.save()
                    return response.json({message: 'Your update Defect Status finish.' })
                  }
                  return response.status(404).send({message: { error:'Resource not found.'} })
              }
              catch (e) {
                  console.log(e);
                  return response.status(404).send({message: { error:'Resource not found.'} })
              }
      }

      async delete({ params, session, response }) {
            const { id } = params
            const defect_status = await DefectStatus.find(id)

              try { 
                  if (!defect_status) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                  } else if (defect_status) {
                  if ( defect_status.deleted == 'D' ) {
                      return response.json({ message: "Current Deleted Status" +" "+ defect_status.defect_status_name +" "+ defect_status.deleted +":Deleted"})
                  }

                  defect_status.deleted = 'D'
                  await defect_status.save()
                  return response.json({message: "Your Deleted Status" +" "+ defect_status.defect_status_name +" "+ defect_status.deleted +":Deleted"})
                  }
              return response.status(404).send({message: { error:'Your deleted Defect Status fail!'} })
          } catch (error) {
              return response.status(400).send({message: { error:'Bad Request!'} })
          }
      }

      async undeleted ({ params, response }) {
            const { id } = params
            const defect_status = await DefectStatus.find(id)

            try { 
                if (!defect_status) {
                  return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (defect_status) {
                if ( defect_status.deleted == 'A' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ defect_status.defect_status_name +" "+ defect_status.deleted +":Active"})
                }

                  defect_status.deleted = 'A'
                  await defect_status.save()
                  return response.json({message: "Your Change Deleted Status" +" "+ defect_status.defect_status_name +" "+ defect_status.deleted +":Active"})
                }
                return response.status(404).send({message: { error:'Your Change Deleted status Defect Status fail!'} })
            } catch (error) {
                return response.status(400).send({message: { error:'Bad Request!'} })
            }
      }
  }

module.exports = DefectStatusSettingController
