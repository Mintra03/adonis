'use strict'

const Servity = use('App/Models/ServitySetting');
const TEP = use('App/Models/TestExecutionPlan')
const Database = use('Database')

class ServitySettingController {

      async servity ({request, auth, response}) {
        const servity = await Servity.all()
          try {
              const showservity = await Database.table('servity_settings')
                                              .innerJoin('test_execution_plans', 'servity_settings.tep_id', 'test_execution_plans.id')
                                              .select('servity_name')
                                              .where('servity_settings.deleted' , 'A')
                                              .whereNot('test_execution_plans.deleted' , 'D')
                  
                  return response.json({ "Servity": servity})
            }
            catch (e) {
              return response.status(404).send({message: { error:'Resource not found.'} })
            }
      }

      async create({request,auth,response}) {
        try {
            const tep_id = request.input("tep_id")
            const servity_name = request.input("servity_name")

            let servity = new Servity()
            servity.created_by = await auth.user.id
            servity.modified_by = await auth.user.id
            servity.tep_id = tep_id
            servity.servity_name = servity_name
            servity.deleted = 'A'
            
            servity = await servity.save()
            return response.json({message: 'Your create Servity finish.' })
          }
          catch (e) {
            return response.status(400).send({message: { error:'Your create Servity fail!'} })
          }
      }

      async update({request, params, auth, response}){
            const { id } = params
            const servity = await Servity.find(id)
              try {
                if (servity) {
                    servity.created_by = await auth.user.id
                    servity.modified_by = await auth.user.id
                    servity.tep_id = request.input("tep_id")
                    servity.servity_name = request.input("servity_name")
                    
                    await servity.save()
                    return response.json({message: 'Your update Servity finish.' })
                  }
                  return response.status(404).send({message: { error:'Resource not found.'} })
              }
              catch (e) {
                  console.log(e);
                  return response.status(404).send({message: { error:'Resource not found.'} })
              }
      }

      async delete({ params, session, response }) {
            const { id } = params
            const servity = await Servity.find(id)

              try { 
                  if (!servity) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                  } else if (servity) {
                  if ( servity.deleted == 'D' ) {
                      return response.json({ message: "Current Deleted Status" +" "+ servity.servity_name +" "+ servity.deleted +":Deleted"})
                  }

                  servity.deleted = 'D'
                  await servity.save()
                  return response.json({message: "Your Deleted Status" +" "+ servity.servity_name +" "+ servity.deleted +":Deleted"})
                  }
              return response.status(404).send({message: { error:'Your deleted Servity fail!'} })
          } catch (error) {
              return response.status(400).send({message: { error:'Bad Request!'} })
          }
      }

      async undeleted ({ params, response }) {
            const { id } = params
            const servity = await Servity.find(id)

            try { 
                if (!servity) {
                  return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (servity) {
                if ( servity.deleted == 'A' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ servity.servity_name +" "+ servity.deleted +":Active"})
                }

                  servity.deleted = 'A'
                  await servity.save()
                  return response.json({message: "Your Change Deleted Status" +" "+ servity.servity_name +" "+ servity.deleted +":Active"})
                }
                return response.status(404).send({message: { error:'Your Change Deleted status Servity fail!'} })
            } catch (error) {
                return response.status(400).send({message: { error:'Bad Request!'} })
            }
      }
}

module.exports = ServitySettingController
