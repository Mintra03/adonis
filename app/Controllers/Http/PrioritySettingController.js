'use strict'

const Priority = use('App/Models/PrioritySetting');
const TEP = use('App/Models/TestExecutionPlan')
const Database = use('Database')

class PrioritySettingController {

      async prioritys ({request, auth, response}) {
        const prioritys = await Priority.all()
          try {
              const priority = await Database.table('priority_settings')
                                              // .innerJoin('test_execution_plans', 'priority_settings.tep_id', 'test_execution_plans.id')
                                              .select('id','priority_name')
                                              .where('priority_settings.deleted' , 'A')
                                              // .whereNot('test_execution_plans.deleted' , 'D')
                  
                  return response.json({ "prioritys":prioritys, "priority": priority})
            } catch (e) {
              return response.status(404).send({message: { error:'Resource not found.'} })
            }
          return response.status(404).send({message: { error:'Resource not found.'} })
      }

      async createPriority({request,auth,response}) {
        try {
              const tep_id = request.input("tep_id")
              const priority_name = request.input("priority_name")

              let priority = new Priority()
              priority.created_by = await auth.user.id
              priority.modified_by = await auth.user.id
              priority.tep_id = tep_id
              priority.priority_name = priority_name
              priority.deleted = 'A'
              
              priority = await priority.save()
              return response.json({message: 'Your create Priority finish.' })
          }
          catch (e) {
              return response.status(400).send({message: { error:'Your create Priority fail!'} })
          }
      }

      async update({request, params, auth, response}){
            const { id } = params
            const priority = await Priority.find(id)
              try {
                if (priority) {
                    priority.created_by = await auth.user.id
                    priority.modified_by = await auth.user.id
                    priority.tep_id = request.input("tep_id")
                    priority.priority_name = request.input("priority_name")
                    
                    await priority.save()
                    return response.json({message: 'Your update Priority finish.' })
                  }
                  return response.status(404).send({message: { error:'Resource not found.'} })
              }
              catch (e) {
                  console.log(e);
                  return response.status(400).send({message: { error:'Bad Request!'} })
              }
      }

      async delete({ params, session, response }) {
            const { id } = params
            const priority = await Priority.find(id)

              try { 
                  if (!priority) {
                      return response.status(404).send({message: { error:'Resource not found.'} })
                  } else if (priority) {
                  if ( priority.deleted == 'D' ) {
                      return response.json({ message: "Current Deleted Status" +" "+ priority.priority_name +" "+ priority.deleted +":Deleted"})
                  }

                  priority.deleted = 'D'
                  await priority.save()
                  return response.json({message: "Your Deleted Status" +" "+ priority.priority_name +" "+ priority.deleted +":Deleted"})
                  }
              return response.status(404).send({message: { error:'Your deleted Priority fail!'} })
          } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
          }
      }

      async undeleted ({ params, response }) {
            const { id } = params
            const priority = await Priority.find(id)

            try { 
                if (!priority) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (priority) {
                if ( priority.deleted == 'A' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ priority.priority_name +" "+ priority.deleted +":Active"})
                }

                  priority.deleted = 'A'
                  await priority.save()
                  return response.json({message: "Your Change Deleted Status" +" "+ priority.priority_name +" "+ priority.deleted +":Active"})
                }
                return response.status(404).send({message: { error:'Your Change Deleted status Priority fail!'} })
            } catch (error) {
                return response.status(400).send({message: { error:'Bad Request!'} })
            }
      }

      
}

module.exports = PrioritySettingController
