'use strict'

const Feature = use('App/Models/Feature');
const Project = use('App/Models/Project');
const Database = use('Database')

class FeatureController {

    async features ({request, auth, response}) {
        const features = await Feature.all()
        try {
            const feature = await Database.table('features')
                                            .innerJoin('projects', 'features.project_id', 'projects.id')
                                            .innerJoin('users', 'features.modified_by','users.id')
                                            .select('features.project_id','features.id','features.feature_name','features.description',
                                            'features.modified_by','users.username','features.created_at','features.updated_at')
                                            .where('features.deleted' , 'A')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('users.status_user', 'D')
                
                return response.json({ "feature": feature})
          }
          catch (e) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }

    async feature ({request, auth, response}) {
        const feature = await Feature.all()
        try {
            const feature = await Database.table('features')
                                            .innerJoin('projects', 'features.project_id', 'projects.id')
                                            .innerJoin('users_has_projects', 'features.project_id', 'users_has_projects.project_id')
                                            .innerJoin('users','users_has_projects.user_id','users.id')
                                            .select('features.project_id','features.id','features.feature_name','features.description',
                                            'features.modified_by','users.username','features.created_at','features.updated_at')
                                            .where('features.deleted' , 'A')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('users_has_projects.deleted' , 'D')
                                            .whereNot('users.status_user', 'D')
                                            .where('users_has_projects.user_id', auth.user.id)
                                            .orWhere('users.role', 'Admin')
                                            .groupBy('features.id')
                
                    return response.json({ "feature": feature})
            } catch (e) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }

    async create({request, auth, response}) {
        try {
            const project_id = request.input("project_id")
            const feature_name = request.input("feature_name")
            const description = request.input("description")
            const status = request.input("status")

            let feature = new Feature()
            feature.created_by = await auth.user.id
            feature.modified_by = await auth.user.id
            feature.project_id = project_id
            feature.feature_name = feature_name
            feature.description = description
            feature.status = status
            
            feature = await feature.save()
            return response.json({message: 'Your create Feature finish.' })
        }
        catch (e) {
            return response.status(400).send({message: { error:'Your create Feature fail.'} })
        }
    }

    async update({request, params, auth, response}) {
            const { id } = params
            const feature = await Feature.find(id)
            
            try {
                if (feature) {
                    feature.created_by = await auth.user.id
                    feature.modified_by = await auth.user.id
                    feature.feature_name = request.input("feature_name")
                    feature.description = request.input("description")

                    await feature.save()
                    return response.json({message: 'Your update Feature finish.' })
                }
                return response.status(400).send({message: { error:'Your update Feature fail.'} })
            }
            catch (e) {
                console.log(e);
                return response.status(404).send({message: { error:'Resource not found.'} })
            }
    }

    async viewfeature ({ params, response ,auth}) {
        const feature = await Feature.findOrFail(params.id);
        try {
            if (!feature) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (feature.deleted == 'A') {
            // const testCase = await feature
            //                 .testCases()
            //                 .fetch()
                                
            const showfeatures = await Database.table('features')
                                        .innerJoin('projects', 'features.project_id', 'projects.id')
                                        .innerJoin('users', 'features.modified_by','users.id')
                                        .select('features.id','features.project_id','features.feature_name','features.created_by',
                                        'features.modified_by','users.username','features.created_at','features.updated_at')
                                        .where('features.deleted' , 'A')
                                        .whereNot('projects.deleted' , 'D')
                                        .whereNot('users.status_user', 'D')
                                        .where('features.id', feature.id)

            // ถ้ามีการอนุญาติ user แล้ว ต้องแก้สิทธิ์เข้าถึง Feature                    
                return response.json({ "Feature": showfeatures})
                }
                return response.status(404).send({message: { error:'Resource not found.'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async changestatus ({ params, request , response }) {
      const { id } = params
      const feature = await Feature.find(id)
      var status = request.input("status")
     
      try { 
          if (!feature) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          } else if (feature) {
          
            if(feature.status == status){
              return response.json({message: "Current feature status"+" "+ feature.feature_name +" : "+ feature.status})
            }

            feature.status = status
            await feature.save()
            return response.json({message: "Your Change Feature Status" +" "+ feature.feature_name +" : "+ feature.status })
          }
        return response.status(404).send({message: { error:'Your Change status Feature fail, Please your ckeck status Feature!'} })
      } catch (error) {
        return response.status(400).send({message: { error:'Bad Request!'} })
      }
    }

    async delete({ params, session, response }) {
            const { id } = params
            const feature = await Feature.find(id)
            try { 
                // const testCase = await feature
                //                 .testCases()
                //                 .fetch()

                if (!feature) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (feature) {
                if ( feature.deleted == 'D' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ feature.feature_name +" "+ feature.deleted +":Deleted"})
                }

                feature.deleted = 'D'
                await feature.save()
                return response.json({message: "Your Deleted Feature Status" +" "+ feature.feature_name +" "+ feature.deleted +":Deleted"})
                }
            
            // await user.delete()
            // session.flash({ notification: 'User Deleted!' })

            return response.status(404).send({message: { error:'Your deleted Feature fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async undeleted ({ params, response }) {
        const { id } = params
        const feature = await Feature.find(id)
        try { 

            if (!feature) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (feature) {
            if ( feature.deleted == 'A' ) {
                return response.json({ message: "Current Deleted Status" +" "+ feature.feature_name +" "+ feature.deleted +":Active"})
            }

            feature.deleted = 'A'
            await feature.save()
            return response.json({message: "Your Change Deleted Status" +" "+ feature.feature_name +" "+ feature.deleted +":Active"})
            }
            return response.status(404).send({message: { error:'Your Change Deleted status Feature fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

}

module.exports = FeatureController
