'use strict'

const User = use('App/Models/User');
const Mail = use('Mail');
const moment = require('moment')
const crypto = require('crypto')

class PasswordResetController {

      showLinkRequestForm({ view }) {
        return view.render('password_reset')
      }

      async sendResetLinkEmail({request, response}) {
          try {
                const user = await User.findBy('email', request.input('email'))
                
                // await PasswordReset.query().where('email', user.email).delete()

                // const { token } = await PasswordReset.create({
                //     email: user.email,
                //     token: randomString({length: 60})
                // })
                const token = await crypto.randomBytes(10).toString('hex')

                // const mailData = {
                //     user: user.toJSON(),
                //     token
                // }
                user.token_created_at = new Date()
                user.token = token
                await user.save()

                await Mail.send('password_reset', {user, token}, (message) => {
                    message
                    .to(user.email)
                    .from('myemail@gmail.com')
                    .subject('Link reset password')
                })
                //   return response.json({ "User": user})
              return response.json({message: 'A password reset link has been sent to your email address.' })
          } catch (e) {
              console.log(e);
              return response.json({message: 'Sorry, there is no user with this email address.'})
          }
      }

    //   async resetpassword({request, response, params}) {
    //     const tokenProvided = params.token
    //     const emailRequesting = params.email

    //     const {newPassword}  = request.input(['newPassword'])
    //     const {confirm_password} = request.input(['confirm_password'])
    //     const user = await User.findByOrFail('email', emailRequesting)
    //     const sameToken = tokenProvided === user.token

    //         if (!sameToken) {
    //             return response
    //             .status(401)
    //             .send({ message: {
    //                 error: 'Old token provided or token already used'
    //             } })
    //         }

    //         const tokenExpired = moment()
    //         .subtract(2, 'days')
    //         .isAfter(user.token_created_at)
        
    //         if (tokenExpired) {
    //         return response.status(401).send({ message: { error: 'Token expired' } })
    //         }
        
    //         user.password = newPassword && confirm_password
    //         user.token = this.token
    //         user.token_created_at = this.timestamps

    //         await user.save()
    //         return response.json({ "User": user}) 

    //     return response.json({message: 'Your password has been reset!'})

    //   }


}

module.exports = PasswordResetController
