'use strict'

const TestCase = use('App/Models/TestCase');
const Database = use('Database')

class TestCaseController {

    async testcases ({request, auth, response}) {
        const testcases = await TestCase.all()
        try {
            const testCases = await Database.table('test_cases')
                                            .innerJoin('projects', 'test_cases.project_id', 'projects.id')
                                            .innerJoin('features', 'test_cases.feature_id', 'features.id')
                                            .innerJoin('users', 'test_cases.modified_by','users.id')
                                            .select('test_cases.id','test_cases.project_id','test_cases.feature_id','test_cases.testcase_No',
                                            'test_cases.testCase_name','test_cases.test_type','test_cases.status','test_cases.priority',
                                            'test_cases.test_by','test_cases.objective','test_cases.precondition','test_cases.test_steps',
                                            'test_cases.expected_results','test_cases.created_by','test_cases.modified_by','users.username',
                                            'test_cases.created_at','test_cases.updated_at')
                                            .where('test_cases.deleted', 'A')
                                            .whereNot('features.deleted' , 'D')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('users.status_user', 'D')
                                            .orderBy('test_cases.id')
                
                return response.json({ "test_cases": testCases})
          }
          catch (e) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }

    async testcase ({request, auth, response}) {
        const testcases = await TestCase.all()
        try {
            const testCase = await Database.table('test_cases')
                                            .innerJoin('projects', 'test_cases.project_id', 'projects.id')
                                            .innerJoin('features', 'test_cases.feature_id', 'features.id')
                                            .innerJoin('users_has_projects', 'test_cases.project_id', 'users_has_projects.project_id')
                                            .innerJoin('users','users_has_projects.user_id','users.id')
                                            .select('test_cases.id','test_cases.project_id','test_cases.feature_id','test_cases.testcase_No',
                                            'test_cases.testCase_name','test_cases.test_type','test_cases.status','test_cases.priority',
                                            'test_cases.test_by','test_cases.objective','test_cases.precondition','test_cases.test_steps',
                                            'test_cases.expected_results','test_cases.created_by','test_cases.modified_by','users.username',
                                            'test_cases.created_at','test_cases.updated_at')
                                            .where('test_cases.deleted', 'A')
                                            .whereNot('features.deleted' , 'D')
                                            .whereNot('projects.deleted' , 'D')
                                            .whereNot('users_has_projects.deleted', 'D')
                                            .whereNot('users.status_user', 'D')
                                            .orderBy('test_cases.id')
                                            .groupBy('test_cases.id')
                                            .where('users_has_projects.user_id', auth.user.id)
                                            .orWhere('users.role', 'Admin')
                
                return response.json({ "test_cases": testCase})
          }
          catch (e) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          }
    }

    async create({request, auth, response}) {
        try {
                const project_id = request.input("project_id")
                const feature_id = request.input("feature_id")
                const testcase_No = request.input("testcase_No")
                const testCase_name = request.input("testCase_name")
                const test_type = request.input("test_type")
                const status = request.input("status")
                const priority = request.input("priority")
                const test_by = request.input("test_by")
                const objective = request.input("objective")
                const precondition = request.input("precondition")
                const test_steps = request.input("test_steps")
                const expected_results = request.input("expected_results")
                const deleted = request.input("deleted")
                
                let testcase = new TestCase()
                testcase.created_by = await auth.user.id
                testcase.modified_by = await auth.user.id
                testcase.testcase_No = testcase_No
                testcase.project_id = project_id
                testcase.feature_id = feature_id
                testcase.testCase_name = testCase_name
                testcase.test_type = test_type
                testcase.status = status
                testcase.priority = priority
                testcase.test_by = test_by
                testcase.objective = objective
                testcase.precondition = precondition
                testcase.test_steps = test_steps
                testcase.expected_results = expected_results
                testcase.deleted = 'A'
                
                testcase = await testcase.save()
                return response.json({message: 'Your create Test Case finish.' })
        }
        catch (e) {
            return response.status(400).send({message: { error:'Your create Test Case fail.'} })
        }
    }

    async update({request, params, auth, response}) {
        console.log(params);
            const { id } = params
            const testcase = await TestCase.find(id)
            
            try {
                if (testcase) {
                    testcase.created_by = await auth.user.id
                    testcase.modified_by = await auth.user.id
                    testcase.testcase_No = request.input("testcase_No")
                    testcase.testCase_name = request.input("testCase_name")
                    testcase.test_type = request.input("test_type")
                    testcase.status = request.input("status")
                    testcase.priority = request.input("priority")
                    testcase.test_by = request.input("test_by")
                    testcase.objective = request.input("objective")
                    testcase.precondition = request.input("precondition")
                    testcase.test_steps = request.input("test_steps")
                    testcase.expected_results = request.input("expected_results")
                    testcase.deleted = request.input("deleted")

                    await testcase.save()
                    return response.json({message: 'Your update Test Case finish.' })
                }
                return response.status(400).send({message: { error:'Your update Test Case fail!'} })
            } catch (e) {
                console.log(e);
                return response.status(404).send({message: { error:'Resource not found.'} })
            }
    }

    async viewtestcase ({ params, response ,auth}) {
        const testcase = await TestCase.findOrFail(params.id);
        try {
            if (!testcase) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (testcase.deleted == 'A') {
            const showTC = await Database.table('test_cases')
                                        .innerJoin('projects', 'test_cases.project_id', 'projects.id')
                                        .innerJoin('features', 'test_cases.feature_id', 'features.id')
                                        .innerJoin('users', 'test_cases.modified_by','users.id')
                                        .select('test_cases.id','test_cases.project_id','test_cases.feature_id','test_cases.testcase_No',
                                        'test_cases.testCase_name','test_cases.test_type','test_cases.status','test_cases.priority',
                                        'test_cases.test_by','test_cases.objective','test_cases.precondition','test_cases.test_steps',
                                        'test_cases.expected_results','test_cases.created_by','test_cases.modified_by','users.username',
                                        'test_cases.created_at','test_cases.updated_at')
                                        .where('test_cases.deleted', 'A')
                                        .whereNot('features.deleted' , 'D')
                                        .whereNot('projects.deleted' , 'D')
                                        .whereNot('users.status_user', 'D')
                                        .where('test_cases.id', testcase.id)
                                        // .orderBy('test_cases.id')

                return response.json({ "test_cases": showTC})
            }
            return response.status(404).send({message: { error:'Resource not found.'} })

          // ถ้ามีการอนุญาติ user แล้ว ต้องแก้สิทธิ์เข้าถึง TestCase
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async changestatus ({ params, request , response }) {
      const { id } = params
      const testcases = await TestCase.find(id)
      var status = request.input("status")
     
      try { 
          if (!testcases) {
            return response.status(404).send({message: { error:'Resource not found.'} })
          } else if (testcases) {
          
            if(testcases.status == status){
              return response.json({message: "Current TestCase Status"+" "+ testcases.testCase_name +" : "+ testcases.status})
            }

            testcases.status = status
            await testcases.save()
            return response.json({message: "Your Change TestCase Status" +" "+ testcases.testCase_name +" : "+ testcases.status })
          }
          return response.status(400).send({message: { error:'Your Change status TestCase fail, Please your ckeck status TestCase!'} })
      } catch (error) {
        return response.status(400).send({message: { error:'Bad Request!'} })
      }
    }

    async delete({ params, session, response }) {
            const { id } = params
            const testcase = await TestCase.find(id)
            try { 

                if (!testcase) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (testcase) {
                if ( testcase.deleted == 'D' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ testcase.testCase_name +" "+ testcase.deleted +":Deleted"})
                }

                testcase.deleted = 'D'
                await testcase.save()
                return response.json({message: "Your Deleted TestCase" +" "+ testcase.testCase_name +" "+ testcase.deleted +":Deleted"})
                }
            return response.status(404).send({message: { error:'Your deleted Test Case fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async undelete ({ params, response }) {
        const { id } = params
        const testcase = await TestCase.find(id)
        try { 

            if (!testcase) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (testcase) {
                if ( testcase.deleted == 'A' ) {
                    return response.json({ message: "Current Deleted Status" +" "+ testcase.testCase_name +" "+ testcase.deleted +":Active"})
                }

                testcase.deleted = 'A'
                    await testcase.save()
                    return response.json({message: "Your Change Deleted Status" +" "+ testcase.testCase_name +" "+ testcase.deleted +":Active"})
                }
            return response.status(404).send({message: { error:'Your Change Deleted status Test Case fail!'} })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

}

module.exports = TestCaseController
