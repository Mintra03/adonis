'use strict'
const User = use('App/Models/User');
const Mail = use('Mail');
const Database = use('Database')
const moment = require('moment')

class UserController {

    async users({request, auth, response}) {
        try {
            const user = await Database.from('users')
                                            .where('users.status_user', 'A')
            return response.json({"users":user})
          }
          catch (e) {
            return response.status(403).send({message: { error:'For admin only!'}})
          }
    }

    async register({request, auth, response}) {
        
            try {
                const username = request.input("username")
                const email = request.input("email")
                const password = request.input("password")
                const confirm_password = request.input("confirm_password")
                const firstname = request.input('firstname')
                const lastname = request.input('lastname')
                const role = request.input('role')
                
                let user = new User()
                user.username = username
                user.email = email
                user.password = password && confirm_password
                user.firstname = firstname
                user.lastname = lastname
                user.role = 'User'

                user = await user.save()
                return response.json({message: 'Your register finish.' })
            }
            catch (e) {
            return response.status(400).send({message: { error:'Your register fail!'} })
            }
    }

    // ใช้ token ในการเข้า 
    async create({request, auth, response}) {
        try {
                const username = request.input("username")
                const email = request.input("email")
                const password = request.input("password")
                const role = request.input("role")
                const firstname = request.input('firstname')
                const lastname = request.input('lastname')
                // const image = request.file("image", {
                //     types: ['image'],
                //     size: '4mb',
                //     extnames: ['png', 'gif', 'jpg']
                // })

                let user = new User()
                // user.image = image
                user.username = username
                user.email = email
                user.password = password 
                user.firstname = firstname
                user.lastname = lastname
                user.role = 'User'

                user = await user.save()
                return response.json({message: 'Your create finish.' })
        }
        catch (e) {
            return response.status(400).send({message: { error:'Your create fail.'} })
        }
    }

    async update({request, params, auth, response}) {
            const { id } = params
            const user = await User.find(id)
            
            try {
                if (user) {
                    // user.image = request.file("image")
                    user.username = request.input("username")
                    user.email = request.input("email")
                    user.password = request.input("password") && request.input("confirm_password")
                    user.firstname = request.input("firstname")
                    user.lastname = request.input("lastname")
                    //Admin Only
                    user.role = request.input("role")

                    await user.save()
                    return response.json({message: 'Your update finish.' })
                }
            }
            catch (e) {
                console.log(e);
                return response.status(400).send({message: { error:'Your update fail.'} })
            }
        return response.status(404).send({message: { error:'Resource not found.'} })
    }

    async view({ params, response ,auth}) {
        const user = await User.findOrFail(params.id);
        
        try {
            if (user) {
                return response.json({user:auth.user});
            }
            // return response.status(403).send({ message: 'Sorry, you do not have permission to access.' });
        } catch (error) {
            return response.status(404).send({message: { error:'Resource not found.'} })
        }
        return response.status(404).send({message: { error:'Resource not found.'} })
    }

    async viewuserproject({params,response}) {
        const { id } = params
        const user = await User.find(id)

        try {
            if (!user) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (user.status_user == 'A') {
            const project = await user
                            .projects()
                            .fetch()
                                            
            const showProject = await Database.table('users_has_projects')
                                        .innerJoin('users', 'users_has_projects.user_id', 'users.id')
                                        .innerJoin('projects', 'users_has_projects.project_id', 'projects.id')
                                        .select('users_has_projects.project_id','projects.project_name','users_has_projects.created_at','users_has_projects.updated_at','projects.created_by','projects.modified_by')
                                        .where('users_has_projects.deleted' , 'A')
                                        .whereNot('users.status_user' , 'D')
                                        .whereNot('projects.deleted', 'D')
                                        .where('users_has_projects.user_id', user.id)
                                            
                return response.json({ "User": user, "Project": showProject})
            }
            return response.json({ "User": user, message: 'User is deleted!'})
        } catch (e) { 
            return response.status(400).send({message: { error:'Bad Request!'} })
        }          
    }

    async delete({ params, session, response }) {
            const { id } = params
            const user = await User.find(id)
            try { 
                if (!user) {
                    return response.status(404).send({message: { error:'Resource not found.'} })
                } else if (user) {
                if ( user.status_user == 'I' ) {
                    return response.json({ message: "Current Status" +" "+ user.username +" "+ user.status_user +":Inactive"})
                }

                user.status_user = 'I'
                await user.save()
                return response.json({message: "Your Change Status" +" "+ user.username +" "+ user.status_user +":Inactive"})
                }
            
            // await user.delete()
            // session.flash({ notification: 'User Deleted!' })
           
                return response.status(404).send({message: 'Your deleted User fail.' })
            } catch (error) {
                return response.status(400).send({message: { error:'Bad Request!'} })
            }
    }

    async changestatus({ params, response }) {
        const { id } = params
        const user = await User.find(id)
        try { 

            if (!user) {
                return response.status(404).send({message: { error:'Resource not found.'} })
            } else if (user) {
                if ( user.status_user == 'A' ) {
                    return response.json({ message: "Current Status" +" "+ user.username +" "+ user.status_user +":Active"})
                }

                user.status_user = 'A'
                await user.save()
                return response.json({message: "Your Change Status" +" "+ user.username +" "+ user.status_user +":Active"})
                }
            return response.status(404).send({message: 'Your Change status User fail, Please your check status User.' })
        } catch (error) {
            return response.status(400).send({message: { error:'Bad Request!'} })
        }
    }

    async resetpassword({request, response, params}) {
        const tokenProvided = params.token
        const emailRequesting = params.email

        const newPassword  = request.input('newPassword')
        const confirm_password = request.input('confirm_password')
        const user = await User.findByOrFail('email', emailRequesting)

        const sameToken = tokenProvided === user.token

            if (!sameToken) {
                return response.status(401).send({ message: {error: 'Old token provided or token already used or resource not found.'} })
            }

            const tokenExpired = moment()
            .subtract(2, 'days')
            .isAfter(user.token_created_at)
        
            if (tokenExpired) {
            return response.status(401).send({ message: { error: 'Token expired' } })
            }
        
            user.password = newPassword && confirm_password
            user.token = null
            user.token_created_at = this.timestamps

            await user.save()
            // return response.json({ "User": user})
            return response.json({message: 'Your password has been reset!'})

      }

    // async sentemail ({ request }) {
    //     const data = request.only(['email', 'username', 'password'])
    //     const user = await User.create(data)
    
    //     await Mail.send('emails.welcome', user.toJSON(), (message) => {
    //       message
    //         .to(user.email)
    //         .from('<from-email>')
    //         .subject('Welcome to Project')
    //     })
        
    //     return 'You sent Email successfully'
    // }
    
}

module.exports = UserController
