'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Project = use("App/Models/Project")
const Freture = use("App/Models/Freture")
const TestCase = use("App/Models/TestCase")

// TODO add unit test for this middleware

class AuthAdminOrOwnerUser {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, auth, response, params }, next, properties) {
    // call next to advance the request
    try {
      await auth.check()
      if (auth.user.role !== 'admin') {
        var userId = null

        switch (properties[0]) {
          case 'project':
            const project = await Project.find(params.id)
            userId = project.user_id
            break
          case 'freture':
            const freture = await Freture.find(params.id)
            userId = freture.user_id
            break
          case 'testCase':
            const testCase = await TestCase.find(params.id)
            userId = testCase.user_id
            break
          default:
            throw new Error(`Middleware [AuthAdminOrOwnerUser] parameter : <${properties[0]}> not implemented`)
        }
        //ผู้สร้างเท่านั้นที่สามารถแก้ไขได้
        if (auth.user.id !== userId) {
          return response.json({ message: "Only the creator of the entry can modify it." })
        }
      }
    } catch (error) {
      return response.json({ message: error.message })
    }
    await next()
  }
}

module.exports = AuthAdminOrOwnerUser
