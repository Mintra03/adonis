'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Users_has_Projects = use('App/Models/Users_has_Projects');
const User = use('App/Models/User')
const Mail = use('Mail');

class Project extends Model {

    users () {
      return this.belongsToMany('App/Models/User','id','project_id','iduser','user_id')
              .pivotTable('Users_has_Projects')
    }

    usersProjects (){
      return this.hasMany('App/Models/Users_has_Projects','id','id','idpj','project_id','iduser','user_id')
    }

    addusersProjects (){
      return this.hasMany('App/Models/Users_has_Projects')
    }

    features() {
      return this.hasMany('App/Models/Feature')
    }

    testCases() {
      return this.hasMany('App/Models/TestCase','id','project_id','idft','feature_id','idtc','id')
    }

    testExecutionPlan() {
      return this.hasMany('App/Models/TestExecutionPlan')
    }

    async saveUser({users,projects,auth}) {
        this.created_by = await auth.user.id 
        this.modified_by = await auth.user.id
        this.project_name = projects.project_name
        this.start_date = projects.start_date
        this.end_date = projects.end_date
        this.description = projects.description
        this.project_manager = projects.project_manager
        this.business_analysis = projects.business_analysis
        this.status = projects.status

        await this.save()
        
        const projectUsers = users
        this.manyInvite(projectUsers)

        //   if(users.id.length <= 1) {
        //     this.oneInvite(users)
        //   } else {
        //     this.manyInvite(users)
        //   }
        
        return
    }

    // async oneInvite(users){
    //     const project = await this
    //     const users_has_Projects = new Users_has_Projects() 
            
    //     users_has_Projects.user_id = users.id
    //     users_has_Projects.project_id = this.id
    //     var status = await users_has_Projects.save()

    //     if(status) {
    //       this.sendemail(users.email)
    //     }
        
    //     return
    // }

    // async manyInvite(users){
    //   var user = []
    //     users.id.forEach(element => {
    //     user.push({"user_id" : element})
    //     });

    async manyInvite(projectUsers,response){
      if(projectUsers != null) {
        var user = projectUsers.reduce((carry, user) => {
          carry.push({"user_id": user.id})
          return carry
        }, [])
      }else {
        return response.status(404).send({message: { error:'Resource not found.'} })
      }
      
        const project = await this
          .addusersProjects()
          .createMany(user)

          if(project != null) {
            projectUsers.forEach(user => {
              this.sendemail(user.email)
            })
          } else {
            return "Sorry, no data was found."
          }
          return
    }

    async sendemail (email) {
        const project = await this
        const project_name = await this.project_name
        
        const user = await User.findBy('email', email)

        await Mail.send('sendemail', { user, project_name }, (message) => {
                message
            .from('myemail@.com')
            .to(email)
            .subject('Welcome to Project')
        })
        return 
    }

}

module.exports = Project
