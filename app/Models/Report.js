'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Report extends Model {

  teps() {
    return this.hasOne('App/Models/TestExecutionPlan')
  }

}

module.exports = Report
