'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Feature extends Model {

    // testCases() {
    //     return this.hasMany('App/Models/TestCase')
    // }

    // testExecutionPlan() {
    //     return this.belongsToMany('App/Models/TestExecutionPlan')
    //                 .pivotTable('Feature_has_Test_excution')
    // }

    // projects() {
    //     return this.hasOne('App/Models/Project','idpj','id')
    // }

    static async getFeature(ids) {
        const features = await this.query().whereIn('id', ids).fetch()
        return features.rows;
    }

}

module.exports = Feature
