'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Feature_has_Test_excution extends Model {

    // users () {
    //     return this.hasMany('App/Models/User')
    //   }

    // teps() {
    //     return this.hasMany('App/Models/TestExecutionPlan')
    // }

    // featuresTEPs() {
    //     return this.hasMany('App/Models/Feature_has_Test_excution')
    // }

    static get table () {
      return 'feature_has_test_excutions'
    }

}

module.exports = Feature_has_Test_excution
