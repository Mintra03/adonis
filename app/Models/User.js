'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })

    // this.addTrait('@provider:Lucid/SoftDeletes')
  }

  // static get hidden() {
  //   return ['password']
  // }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  projects() {
    return this.belongsToMany('App/Models/Project')
              .pivotTable('users_has_projects')
  }

  testExecutionPlan() {
    return this.belongsToMany('App/Models/TestExecutionPlan')
              // .pivotTable('Executions_has_Users')
  }

  static async getUsers(ids) {
    const users = await this.query().whereIn('id', ids).fetch()
    return users.rows;
  }

}

module.exports = User
