'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ExecutionsHasTesCase extends Model {

  users () {
    return this.hasMany('App/Models/User')
  }

  features() {
    return this.hasMany('App/Models/Feature')
  }
  
  testResult() {
    return this.hasMany('App/Models/TestResult')
  }

  teps() {
    return this.hasMany('App/Models/TestExecutionPlan')
  }

  testcase() {
    return this.hasMany('App/Models/TestCase')
  }

}

module.exports = ExecutionsHasTesCase
