'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ResetPassword extends Model {
}

module.exports = ResetPassword
