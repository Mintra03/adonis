'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class History extends Model {

  teps() {
    return this.hasOne('App/Models/TestExecutionPlan')
  }

  testCasesTEPs() {
    return this.hasOne('App/Models/ExecutionsHasTesCase','id','tep_id','idft','feature_id','idtctep','test_cases_id','idtc','id')
  }

}

module.exports = History
