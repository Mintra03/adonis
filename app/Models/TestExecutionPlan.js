'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const ExecutionsHasUser = use('App/Models/ExecutionsHasUser')
const FeatureHasTestExcution = use('App/Models/Feature_has_Test_excution')
const ExecutionsHasTesCase = use('App/Models/ExecutionsHasTesCase')
const Database = use('Database')
const User = use('App/Models/User')
const Testcase = use('App/Models/TestCase')
const History = use('App/Models/History')

class TestExecutionPlan extends Model {

    users () {
      return this.belongsToMany('App/Models/User')
              .pivotTable('executions_has_users')
    }

    usersTeps() {
      return this.hasMany('App/Models/ExecutionsHasUser')
    }

    projects() {
      return this.hasMany('App/Models/Project','idpj','id')
    }

    features() {
      return this.belongsToMany('App/Models/Feature')
              .pivotTable('feature_has_test_excutions')
    }

    featuresTEPs() {
      return this.hasMany('App/Models/Feature_has_Test_excution','id','tep_id','idft','feature_id','idpj','project_id')
    }

    addfeaturesTEPs() {
      return this.hasMany('App/Models/Feature_has_Test_excution','id','tep_id','idft','feature_id','idpj','project_id')
    }

    testCases() {
      return this.belongsToMany('App/Models/TestCase')
              .pivotTable('executions_has_tes_cases')
    }

    testCasesTEPs() {
      return this.hasMany('App/Models/ExecutionsHasTesCase','id','tep_id','idft','feature_id','idtctep','test_cases_id','idtc','id','idpj','project_id')
    }

    testResults() {
      return this.hasMany('App/Models/TestResult')
    }

    historys() {
      return this.hasOne('App/Models/History','id','TephTc_id','idts','test_result_id')
    }

    async saveUser ({project,teps,auth,users}) {
      
        this.created_by = await auth.user.id
        this.modified_by = await auth.user.id
        this.project_id = project.id
        this.tep_name = teps.tep_name
        this.description = teps.description
        this.priority = teps.priority
        this.test_type = teps.test_type
        this.test_by = teps.test_by
        this.status = 'Active'

        await this.save()
              // this.user(users)
              // this.testcase(testCases)

              // const tepUsers = users
              // this.manyInvite(tepUsers)    

        return
    }

    // async manyInvite(tepUsers,response){
    //   if(tepUsers != null) {
    //     var user = tepUsers.reduce((carry, user) => {
    //       carry.push({"user_id": user.id})
    //       return carry
    //     }, [])
    //   }else {
    //     return response.status(404).send({message: { error:'Resource not found.'} })
    //   }
      
    //     const project = await this
    //       .usersTeps()
    //       .createMany(user)

    //       if(project != null) {
    //         tepUsers.forEach(user => {
    //           this.sendemail(user.email)
    //         })
    //       } else {
    //         return "Sorry, no data was found."
    //       }
    //       return
    // }

    // async user(users) {
    //     const tep = await this
    //     const tep_has_users = new ExecutionsHasUser()

    //     tep_has_users.user_id = users.id
    //     tep_has_users.tep_id = this.id
    //     await tep_has_users.save()
        
    //     return
    // }

    async saveFeature({teps,features,auth,project}) {
        
            // if(features.id.length <= 1) {
            //     const featuresstatus = await Database.from('features').select('id').where('status', 'Inactive').where('id','in', [features.id])
            //       if(featuresstatus.length) {
            //         return 'Can not add feature because it is not an active status!'
            //       } 
            //     this.onefeature(teps,features,auth)
            //     return 'Save one Feature has TestExcution success';
            // } else {
            //     const featuresstatus2 = await Database.from('features').select('id').where('status', 'Inactive').where('id','in', features.id)
            //       if(featuresstatus2.length) {
            //         return 'Can not add many feature because it is not an active status!'
            //       }
                this.manyfeature(teps,features,auth,project)
            //     return 'Save many Feature has TestExcution  success';
            // } 
            return
    }

    // async onefeature(teps,features,auth) {
    //     const tep = await this
    //     const feature_has_test_excutions = new FeatureHasTestExcution()

    //     feature_has_test_excutions.feature_id = features.id
    //     feature_has_test_excutions.tep_id = teps.id
    //     feature_has_test_excutions.created_by = await auth.user.id
    //     feature_has_test_excutions.modified_by = await auth.user.id

    //     return await feature_has_test_excutions.save()
    // }

    async manyfeature(teps,features,auth,project) {
      var feature = features.reduce((carry, feature) => {
          carry.push({"feature_id":feature.id,"project_id":project.id,"tep_id":teps.id,"created_by":auth.user.id,"modified_by":auth.user.id})
          return carry
        },[])

        const tep = await this
                .addfeaturesTEPs()
                .createMany(feature)
      return 
    }

    async saveTestCase({teps,features,auth,idtestcases,testresults,projects}) {
          const tep = await this
 
   
        //   //Testcase ที่อยู่ใต้ Feature
            const testcaseFT = await Database.from('test_cases').select('feature_id', 'id').where('id','in', idtestcases)
              if(!testcaseFT.length) {
                return 'Can not add testcase because it is not in this feature!'
              }

            const testcasestatus = await Database.from('test_cases').select('id').where({status:['Obsolete']}).where('id','in', idtestcases)
              if(testcasestatus.length) {
                return 'Can not add testcase because it is not an active or new status!'
              } 
              
              console.log(idtestcases);

              const testcases = await Testcase.getTestcase(idtestcases)

              // console.log(testcases);
              // return
              
              this.manytestcase(teps,features,testcases,auth,testresults,projects)
        //     return 'Save one TestCase has TestExcution success';
      
        return
    }

    // async onetestcase(teps,features,testcases,auth,testresults) {
    //     const tep = await this
    //     const tep_has_testCases = new ExecutionsHasTesCase()

    //     tep_has_testCases.tep_id = teps.id
    //     tep_has_testCases.feature_id = features.id
    //     tep_has_testCases.test_cases_id = testcases.id
    //     tep_has_testCases.test_result_id = testresults.id
    //     tep_has_testCases.created_by = await auth.user.id
    //     tep_has_testCases.modified_by = await auth.user.id
    //     tep_has_testCases.test_type = testcases.test_type
    //     tep_has_testCases.status = testcases.status 
        
    //     // return await tep_has_testCases.save()
    //     console.log(tep_has_testCases.test_type);
    //     return
    // }

    async manytestcase(teps,features,testcases,auth,testresults,projects) {

      var testcase = testcases.reduce((carry, testcase) => {
          carry.push({"test_cases_id": testcase.id,"project_id":projects.id, "tep_id": teps.id, "feature_id": features.id, 
          "test_result_id": testresults.id, "test_type": testcase.test_type, "status": testcase.status, "created_by": auth.user.id, "modified_by": auth.user.id })
          return carry
      },[])

      // const getdata = await Database.table('test_cases')
      //                           .columnInfo('test_type','status')
      //                           .where('test_cases.deleted','A')
      //                           .where('test_cases.id', testcase)

      //     const dataid = await Testcase.findBy(getdata[0])
      //                           console.log(dataid);
      //                           return
                                
          const tep = await this
              .testCasesTEPs()
              .createMany(testcase) 

      return
    }

}

module.exports = TestExecutionPlan
