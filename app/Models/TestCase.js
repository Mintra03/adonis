'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TestCase extends Model {

    testExecutionPlan() {
        return this.belongsToMany('App/Models/TestExecutionPlan')
                    // .pivotTable('executions_has_tes_cases')
    }

    testSteps() {
        return this.hasMany('App/Models/TestStep')
    }

    static async getTestcase(ids) {
        const testcases = await this.query().whereIn('id', ids).fetch()
        return testcases.rows;
    }

    // static async getTCedit(ids,test_type,status) {
    //     const testcases = await this.query().whereIn('id', ids, 'test_type',test_type, 'status',status).fetch()
    //     return testcases.rows;
    // }
}

module.exports = TestCase
