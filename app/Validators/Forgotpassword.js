'use strict'

class Forgotpassword {
  get rules () {
    return {
      email: 'required|email',
    }
  }

  get messages() {
    return {
      'email.required': 'Sorry, there is no user with this email address.',
      'email.email': 'Please provide valid email format.',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
  
}

module.exports = Forgotpassword
