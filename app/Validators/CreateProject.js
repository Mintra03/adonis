'use strict'

class CreateProject {
  get rules () {
    return {
      project_name: 'required|unique:projects,project_name',
      status: 'required',
      //  software test engineer
    }
  }

  get messages () {
    return {
    'project_name.required': 'Please provide project_name.',
    'project_name.unique': 'This project name has already been used.',
    'status.required': 'Please provide project status',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
}

module.exports = CreateProject
