'use strict'

class CreateUser {
    get rules () {
      return {
        username: 'required|unique:users,username',
        email: 'required|email|unique:users',
        password: 'required|min:8',
      }
    }

    get messages () {
      return {
      'username.required': 'Please provide username.',
      'username.unique': 'Username must be unique.',
      'email.required': 'You must provide a email address.',
      'email.email': 'You must provide a valid email format.',
      'email.unique': 'This email is already registered.',
      'password.required': 'You must provide a password',
      'password.min': 'Password must contain at least 1 number alphabet ,special character and minimum length is 8',
      }
    }
  
    async fails(error) {
      return this.ctx.response.send(error);
    }

}

module.exports = CreateUser
