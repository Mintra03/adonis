'use strict'

class CreateFeature {
  get rules () {
    return {
      feature_name: 'required|unique:features,feature_name',
      status: 'required'
    }
  }

  get messages () {
    return {
    'feature_name.required': 'Please provide feature name.',
    'feature_name.unique': 'Feature Name must be unique.',
    'status.required': 'Please provide feature status',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
}

module.exports = CreateFeature
