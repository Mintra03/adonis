'use strict'

class UpdateTEP {
    get rules () {
      return {
        status: 'required',
      }
    }

    get messages () {
      return {
      'status.required': 'Please provide test execution plan status.',
      }
    }

    async fails(error) {
      return this.ctx.response.send(error);
    }

}

module.exports = UpdateTEP
