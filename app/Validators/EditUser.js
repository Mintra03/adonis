'use strict'

class EditUser {
  get rules () {
    return {
      firstname: 'required',
      lastname: 'required',
      password: 'required|min:8',
      confirm_password: 'required_if:password|same:password'
    }
  }

  get messages () {
    return {
    'firstname.required': 'You must provide firstname.',
    'lastname.required': 'You must provide lastname.',
    'password.required': 'Please provide Password.',
    'password.min': 'Password must contain at least 1 number alphabet ,special character and minimum length is 8.',
    'confirm_password.required_if': 'Please provide Confirm Password.',
    'confirm_password.same': 'Confirm Password fields does not match.'
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
}

module.exports = EditUser
