'use strict'

class EditTestCase {
  get rules () {
    return {
      testcase_No: 'required',
      testCase_name: 'required',
      test_type: 'required',
      priority: 'required',
      expected_results: 'required'
    }
  }

  get messages () {
    return {
    'testcase_No.required': 'Please provide Test Case No.',
    'testCase_name.required': 'Please provide Test Case Name.',
    'test_type.required': 'Please provide Test Type.',
    'priority.required': 'Please provide Priority.',
    'expected_results.required': 'Please provide Expected Results.',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
  
}

module.exports = EditTestCase
