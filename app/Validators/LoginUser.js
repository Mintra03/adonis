'use strict'

class LoginUser {
  get rules () {
    return {
      email: 'required|email',
      password: 'required'
    }
  }

  get messages() {
    return {
      'email.required': 'Please provide email.',
      'password.required': 'Please provide password.',
      'required': 'Login fail, invalid email or password.',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error)
  }
}

module.exports = LoginUser
