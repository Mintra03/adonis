'use strict'

class EditFeature {
  get rules () {
    return {
      feature_name: 'required',
      status: 'required'
    }
  }

  get messages () {
    return {
    'feature_name.required': 'Please provide feature name.',
    'status.required': 'Please provide feature status',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
}

module.exports = EditFeature
