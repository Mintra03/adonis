'use strict'

class CreateTestCase {
  get rules () {
    return {
      testcase_No: 'required|unique:test_cases,testcase_No',
      testCase_name: 'required',
      test_type: 'required',
      priority: 'required',
      expected_results: 'required'
    }
  }

  get messages () {
    return {
    'testcase_No.required': 'Please provide Test Case No.',
    'testcase_No.unique': 'testcase_No must be unique.',
    'testCase_name.required': 'Please provide Test Case Name.',
    'test_type.required': 'Please provide Test Type.',
    'priority.required': 'Please provide Priority.',
    'expected_results.required': 'Please provide Expected Results.',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
}

module.exports = CreateTestCase
