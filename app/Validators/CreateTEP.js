'use strict'

class CreateTEP {
    get rules () {
      return {
        // project_id: 'required',
        tep_name: 'required',
        description:  'required',
        priority:  'required',
        test_type:  'required',
        test_by:  'required',
      }
    }

    get messages () {
      return {
      // 'project_id.required': 'Please provide project ID.',
      'tep_name.required': 'Please provide test execution plan name.',
      'description.required': 'Please provide test execution plan description.',
      'priority.required': 'Please provide test execution plan priority.',
      'test_type.required': 'Please provide test execution plan Test Type.',
      'test_by.required': 'Please provide test execution plan Test By.',
      }
    }

    async fails(error) {
      return this.ctx.response.send(error);
    }

}

module.exports = CreateTEP
