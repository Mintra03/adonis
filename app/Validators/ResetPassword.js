'use strict'

class ResetPassword {
  get rules () {
    return {
      newPassword: 'required|min:8',
      confirm_password: 'required_if:newPassword|same:newPassword'
    }
  }

  get messages() {
    return {
      'newPassword.required': 'Please provide Password',
      'newPassword.min': 'Password must contain at least 1 number alphabet ,special character and minimum length is 8',
      'confirm_password.required_if:newPassword': 'Confirm Password fields does not match.'
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }

}

module.exports = ResetPassword
