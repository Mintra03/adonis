'use strict'

class RegisterUser {

  get rules () {
    return {
      username: 'required|unique:users,username',
      email: 'required|unique:users,email',
      password: 'required|min:8',
      confirm_password: 'required_if:password|same:password'
    }
  }

  get messages() {
    return {
      'username.required': 'Please provide username.',
      'username.unique': 'Username must be unique.',
      'email.required': 'Please provide valid email format.',
      'email.email': 'Please provide valid email format.',
      'email.unique': 'Email must be unique.',
      'password.required': 'Please provide Password.',
      'password.min': 'Password must contain at least 1 number alphabet ,special character and minimum length is 8.',
      'confirm_password.required_if': 'Please provide Confirm Password.',
      'confirm_password.same': 'Confirm Password fields does not match.'
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
}

module.exports = RegisterUser
