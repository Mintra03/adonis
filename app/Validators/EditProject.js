'use strict'

class EditProject {
  get rules () {
    return {
      project_name: 'required',
      status: 'required',
      //  software test engineer
    }
  }

  get messages () {
    return {
    'project_name.required': 'Please provide project_name.',
    'status.required': 'Please provide project status',
    }
  }

  async fails(error) {
    return this.ctx.response.send(error);
  }
}

module.exports = EditProject
