'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Helpers = use('Helpers')
const edge = use('edge.js')

// Route.on('/').render('welcome')
Route.get('/', () => {
  return { greeting: 'Welcome to Project' }
})

//User
Route.post('/auth/register', 'UserController.register').validator('RegisterUser')
Route.post('/auth/login', 'AuthController.login').validator('LoginUser')
Route.post('/auth/logout', 'AuthController.logout')

Route.get('/user', 'UserController.users').middleware(['auth','authAdmin'])
Route.get('/user/:id', 'UserController.view').middleware(['auth'])
Route.get('/user/:id/project', 'UserController.viewuserproject').middleware(['auth'])
Route.post('/user', 'UserController.create').middleware(['auth','authAdmin']).validator('CreateUser')
Route.put('/user/:id', 'UserController.update').middleware(['auth']).validator('EditUser')
// Route.put('/updateuser/:id', 'UserController.update').middleware(['auth','authAdmin'])
Route.delete('/user/:id', 'UserController.delete').middleware(['auth','authAdmin'])
Route.put('/statusdeletedUser/:id', 'UserController.changestatus').middleware(['auth','authAdmin'])

//ForgotPassword
Route.get('/password/reset', 'PasswordResetController.showLinkRequestForm')
Route.post('/password/email', 'PasswordResetController.sendResetLinkEmail').validator('Forgotpassword') 
Route.get('/password/reset/:token', 'PasswordResetController.showResetForm')
Route.post('/password/reset/:token/:email', 'UserController.resetpassword').validator('ResetPassword')  

//Project
Route.get('/projects', 'ProjectController.projects').middleware(['auth','authAdmin'])
Route.get('/project', 'ProjectController.project').middleware(['auth'])
Route.get('/project/:id', 'ProjectController.viewproject').middleware(['auth'])
Route.post('/project', 'ProjectController.create').middleware(['auth']).validator('CreateProject')
Route.put('/project/:id', 'ProjectController.update').middleware(['auth']).validator('EditProject')
Route.put('/statusProject/:id', 'ProjectController.changestatus').middleware(['auth'])
Route.delete('/project/:id', 'ProjectController.delete').middleware(['auth'])
Route.put('/statusdeletedProject/:id', 'ProjectController.undeleted').middleware(['auth'])
Route.get('/user/project/:id', 'ProjectController.viewUserProject').middleware(['auth'])
Route.get('/project/:id/feature', 'ProjectController.viewFeatureProject').middleware(['auth'])
Route.get('/project/:id/feature/:idft/testcase', 'ProjectController.viewTestCaseProject').middleware(['auth'])
Route.get('/project/:id/feature/:idft/testcase/:idtc', 'ProjectController.viewAll').middleware(['auth'])
Route.delete('/project/:id/user/:iduser', 'ProjectController.deleteUserProject').middleware(['auth'])
Route.post('/project/user/:id', 'ProjectController.addUserinProject').middleware(['auth'])

//Feature
Route.get('/features', 'FeatureController.features').middleware(['auth','authAdmin'])
Route.get('/feature', 'FeatureController.feature').middleware(['auth'])
Route.get('/feature/:id', 'FeatureController.viewfeature').middleware(['auth'])
Route.post('/feature', 'FeatureController.create').middleware(['auth']).validator('CreateFeature')
Route.put('/feature/:id', 'FeatureController.update').middleware(['auth']).validator('EditFeature')
Route.put('/statusFeature/:id', 'FeatureController.changestatus').middleware(['auth'])
Route.delete('/feature/:id', 'FeatureController.delete').middleware(['auth'])
Route.put('/statusdeletedFeature/:id', 'FeatureController.undeleted').middleware(['auth'])

//TestCase
Route.get('/testcases', 'TestCaseController.testcases').middleware(['auth','authAdmin'])
Route.get('/testcase', 'TestCaseController.testcase').middleware(['auth'])
Route.get('/testcase/:id', 'TestCaseController.viewtestcase').middleware(['auth'])
Route.post('/testcase', 'TestCaseController.create').middleware(['auth']).validator('CreateTestCase')
Route.put('/testcase/:id', 'TestCaseController.update').middleware(['auth']).validator('EditTestCase')
Route.put('/statusTestcase/:id', 'TestCaseController.changestatus').middleware(['auth'])
Route.delete('/testcase/:id', 'TestCaseController.delete').middleware(['auth'])
Route.put('/statusdeletedTestcase/:id', 'TestCaseController.undelete').middleware(['auth'])

//Invite
Route.post('/inputId', 'UserProjectController.invite').middleware(['auth'])

//SendEmail
Route.post('/sendemail', 'ProjectController.sendemail').middleware(['auth','authAdmin'])

//TestExecutionPlan
Route.get('/project/:idpj/teps', 'TepController.teps').middleware(['auth','authAdmin'])
Route.get('/project/:idpj/tep', 'TepController.tep').middleware(['auth'])
Route.get('/project/:idpj/tep/:id', 'TepController.viewTEP').middleware(['auth'])
Route.post('/project/:idpj/tep', 'TepController.createTEP').middleware(['auth']).validator('CreateTEP')
Route.put('/project/:idpj/tep/:id', 'TepController.updateTEP').middleware(['auth']).validator('UpdateTEP')
Route.put('/project/:idpj/statusTEP/:id', 'TepController.changestatusTEP').middleware(['auth'])
Route.delete('/project/:idpj/tep/:id', 'TepController.deleteTEP').middleware(['auth'])
Route.put('/project/:idpj/statusdeletedTEP/:id', 'TepController.undeletedTEP').middleware(['auth'])

//TEPFeature
Route.get('/project/:idpj/TEP/:id/listfeature', 'TepController.listfeature').middleware(['auth'])
Route.get('/project/:idpj/TEP/:id/feature', 'TepController.featuresTEPs').middleware(['auth'])
Route.get('/project/:idpj/TEP/:id/feature/:idft', 'TepController.featureTEPs').middleware(['auth'])
Route.post('/project/:idpj/TEP/:id/feature', 'TepController.addFeature').middleware(['auth'])
Route.delete('/project/:idpj/TEP/:id/feature/:idft', 'TepController.deletefeatureTEP').middleware(['auth'])
Route.put('/project/:idpj/TEP/:id/feature/:idft', 'TepController.undeletedfeatureTEP').middleware(['auth'])

//TEPTestCase
Route.post('/project/:idpj/TEP/:id/feature/:idft/testcase', 'TepController.addTestCase').middleware(['auth'])
Route.post('/project/:idpj/TEP/:id/feature/:idft/testcase/:idtc', 'TepController.updateTestCase').middleware(['auth'])
Route.get('/project/:idpj/TEP/:id/feature/:idft/testcase', 'TepController.testcasesTEPs').middleware(['auth'])
Route.get('/project/:idpj/TEP/:id/feature/:idft/testcase/:idtctep', 'TepController.testcaseTEPs').middleware(['auth'])
Route.delete('/project/:idpj/TEP/:id/feature/:idft/testcase/:idtctep', 'TepController.deletedTCTEP').middleware(['auth'])
Route.put('/project/:idpj/TEP/:id/feature/:idft/testcase/:idtctep', 'TepController.undeletedTCTEP').middleware(['auth'])
Route.get('/project/:idpj/history/TEP/:id/feature/:idft/testcase/:idtc', 'TepController.viewHistory').middleware(['auth'])


//TestResult
Route.get('/testresult', 'TestResultController.testResults').middleware(['auth'])
Route.get('/testresult/:id', 'TestResultController.viewtestresult').middleware(['auth'])
Route.post('/testresult', 'TestResultController.create').middleware(['auth'])
Route.put('/testresult/:id', 'TestResultController.update').middleware(['auth'])
Route.delete('/testresult/:id', 'TestResultController.delete').middleware(['auth'])
Route.put('/statusdeletedtestresult/:id', 'TestResultController.undelete').middleware(['auth'])

//Report
Route.get('/Testprogress/TEP/:id', 'ReportController.testprogress').middleware(['auth'])
Route.get('/Priority/TEP/:id', 'ReportController.OpenPriority').middleware(['auth'])
Route.get('/Severity/TEP/:id', 'ReportController.OpenSeverity').middleware(['auth'])
Route.get('/DefectSeverity/TEP/:id', 'ReportController.OpendefectSeverity').middleware(['auth'])

//PrioritySetting
Route.get('/priority', 'PrioritySettingController.prioritys').middleware(['auth','authAdmin'])
Route.post('/priority', 'PrioritySettingController.createPriority').middleware(['auth'])
Route.put('/priority/:id', 'PrioritySettingController.update').middleware(['auth'])
Route.delete('/priority/:id', 'PrioritySettingController.delete').middleware(['auth'])
Route.put('/statusdeletedpriority/:id', 'PrioritySettingController.undeleted').middleware(['auth'])

//ServitySetting
Route.get('/servity', 'ServitySettingController.servity').middleware(['auth','authAdmin'])
Route.post('/servity', 'ServitySettingController.create').middleware(['auth'])
Route.put('/servity/:id', 'ServitySettingController.update').middleware(['auth'])
Route.delete('/servity/:id', 'ServitySettingController.delete').middleware(['auth'])
Route.put('/statusdeletedservity/:id', 'ServitySettingController.undeleted').middleware(['auth'])

//DefectStatusSetting
Route.get('/defectStatus', 'DefectStatusSettingController.defectStatus').middleware(['auth','authAdmin'])
Route.post('/defectStatus', 'DefectStatusSettingController.create').middleware(['auth'])
Route.put('/defectStatus/:id', 'DefectStatusSettingController.update').middleware(['auth'])
Route.delete('/defectStatus/:id', 'DefectStatusSettingController.delete').middleware(['auth'])
Route.put('/statusdeleteddefectStatus/:id', 'DefectStatusSettingController.undeleted').middleware(['auth'])


Route.any("*", ({ response }) =>
  response.json({
    message: "route not found",
    help: "https://gitlab.com/CrBast/website/wikis/home"
  })
)

.prefix("api")
.formats(["json"])
